﻿using System;
using BiblioData.Context;
using BiblioLogger;
using BiblioSearchFB2.Searcher;
using BiblioService;
using BiblioService.Creator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;

namespace BiblioServiceTest
{
    [TestFixture]
    public class ServiceFB2Test
    {
        [Test]
        public void ConstructorTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(false);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 actual = new ServiceFB2(true, creator.Object);

            dataOperation.Verify(d => d.ExistsDb());
            searchController.Verify(s => s.Execute(@"C:\"));

        }

        [Test]
        public void Constructor2Test()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(false);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 actual = new ServiceFB2(false, creator.Object);

            dataOperation.Verify(d => d.ExistsDb());
            searchController.Verify(s => s.Execute(@"C:\"));
            biblioLog.Verify(l => l.Info(It.IsAny<string>()));
        }

        [Test]
        public void Constructor3Test()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(true);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 actual = new ServiceFB2(false, creator.Object);

            dataOperation.Verify(d => d.ExistsDb());
            searchController.Verify(s => s.Execute(@"C:\"), Times.Never);
            biblioLog.Verify(l => l.Info(It.IsAny<string>()));
        }

        [Test]
        public void Constructor4Test()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");
            setting.SetupGet(s => s.TimerFileSearch).Returns(true);

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(true);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 actual = new ServiceFB2(true, creator.Object);

            dataOperation.Verify(d => d.ExistsDb());
            searchController.Verify(s => s.Execute(@"C:\"), Times.Never);

        }

        [Test]
        public void OnChangedCoreTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(true);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 service = new ServiceFB2(false, creator.Object);

            service.OnChangedCore(new System.IO.FileSystemEventArgs(System.IO.WatcherChangeTypes.Deleted, @"D:\Temp", "test.fb2"));
            searchController.Verify(s => s.ProcessFileIfRemoved(@"D:\Temp\test.fb2"));

            service.OnChangedCore(new System.IO.FileSystemEventArgs(System.IO.WatcherChangeTypes.Created, @"D:\Temp", "test.fb2"));
            searchController.Verify(s => s.ProcessFile(@"D:\Temp\test.fb2"));
        }

        [Test]
        public void OnRenamedCoreTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(@"C:\");

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(true);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);

            ServiceFB2 service = new ServiceFB2(false, creator.Object);

            service.OnRenamedCore(new System.IO.RenamedEventArgs(System.IO.WatcherChangeTypes.Renamed, @"D:\Temp", "test.fb2", "test.oth"));
            searchController.Verify(s => s.ProcessFile(@"D:\Temp\test.fb2"));

            service.OnRenamedCore(new System.IO.RenamedEventArgs(System.IO.WatcherChangeTypes.Renamed, @"D:\Temp", "test.oth", "test.fb2"));
            searchController.Verify(s => s.ProcessFileIfRemoved(@"D:\Temp\test.fb2"));

            service.OnRenamedCore(new System.IO.RenamedEventArgs(System.IO.WatcherChangeTypes.Renamed, @"D:\Temp", "test.fb2", "test1.fb2"));
            searchController.Verify(s => s.ProcessFileIfRenamed(@"D:\Temp\test1.fb2", @"D:\Temp\test.fb2"));


        }

        [Test]
        public void OnEventTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var biblioLog = repo.Create<IBiblioLog>();

            string path = System.IO.Path.GetTempPath();

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.BookCatalogue).Returns(path);

            var dataOperation = repo.Create<IDataOperation>();
            dataOperation.Setup(d => d.ExistsDb()).Returns(true);

            var searchController = repo.Create<ISearchController>();

            var creator = repo.Create<ICreator>();
            creator.Setup(c => c.CreateBiblioLog()).Returns(biblioLog.Object);
            creator.Setup(c => c.CreateDataOperation()).Returns(dataOperation.Object);
            creator.Setup(c => c.CreateSettings()).Returns(setting.Object);
            creator.Setup(c => c.CreateSearchController()).Returns(searchController.Object);


            string fileName = System.IO.Path.Combine(path, "test.fb2");
            string fileName2 = System.IO.Path.Combine(path, "test2.fb2");
            if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);
            if (System.IO.File.Exists(fileName2)) System.IO.File.Delete(fileName2);

            ServiceFB2 service = new ServiceFB2(false, creator.Object);

            service.Start();
            
            var fs = System.IO.File.Create(fileName);
            fs.Close();
            searchController.Verify(s => s.ProcessFile(fileName));

            System.IO.File.Move(fileName, fileName2);
            searchController.Verify(s => s.ProcessFileIfRenamed(fileName, fileName2));

            System.IO.File.Delete(fileName2);
            searchController.Verify(s => s.ProcessFileIfRemoved(fileName2));

            service.Stop();
            
        }



    }
}
