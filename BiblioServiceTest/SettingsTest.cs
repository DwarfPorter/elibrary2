﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiblioService;
using NUnit.Framework;

namespace BiblioServiceTest
{
    [TestFixture]
    public class SettingsTest
    {
        [Test]
        public void TimerFileSearchTest()
        {
            var setting = new Settings();
            bool actualFileSearcher = setting.TimerFileSearch;
            Assert.IsFalse(actualFileSearcher);
        }

        [Test]
        public void ExecuteTimeTest()
        {
            var setting = new Settings();
            var actualExecuteTime = setting.ExecuteTime;
            Assert.AreEqual(13, actualExecuteTime.Value.Hours);
            Assert.AreEqual(0, actualExecuteTime.Value.Minutes);
        }

        [Test]
        public void BookCatalogueTest()
        {
            var setting = new Settings();
            var actualBookCatalogue = setting.BookCatalogue;
            Assert.IsFalse(string.IsNullOrWhiteSpace(actualBookCatalogue));
        }


    }
}
