﻿using BiblioData.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioDataConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());

            var biblioContext = new BiblioContext();

            Console.WriteLine(Directory.GetCurrentDirectory());

            foreach (var item in biblioContext.Authors)
            {
                Console.WriteLine(item.FirstName);
            }

            Console.WriteLine("Master Authors");

            using (DataOperation dbo = new DataOperation())
            {
                foreach (var item in dbo.GetMasterAuthors())
                {
                    Console.WriteLine(item.AuthorId + " | " + item.MasterAuthorId + " | " + item.FirstName);
                }

            }



            Console.WriteLine("Press Enter");
            Console.ReadLine();

        }
    }
}
