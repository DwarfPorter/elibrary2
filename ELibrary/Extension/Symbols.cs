﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Extension
{
    public static class Symbols
    {
        public const string Space = " ";
        public const string GuiDelimiter = ", ";
        public const string Ellipsis = "…";

        public static string AddOneSpaceOrEmptyIfNull(this string str)
        {
            return str == null ? string.Empty : str + Space;
        }
    }
}
