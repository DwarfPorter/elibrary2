﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BiblioData;

namespace ELibrary.Mapper
{
    public static class InitMapper
    {
        public static IConfigurationProvider CatBookMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BiblioData.DbModels.Author, Models.CatBookForm.AuthorForCatBook>();
                cfg.CreateMap<BiblioData.DbModels.Book, Models.CatBookForm.CatBook>();
            });
            return config;
        }

        public static IConfigurationProvider BookMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BiblioData.DbModels.Author, Models.BookForm.AuthorForBook>();
                cfg.CreateMap<BiblioData.DbModels.Content, Models.BookForm.ContentForBook>();
                cfg.CreateMap<BiblioData.DbModels.Book, Models.BookForm.Book>();
            });
            return config;
        }

        public static IConfigurationProvider CatAuthorMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BiblioData.DbModels.MasterGroupAuthor, Models.CatAuthorForm.CatAuthor>();
            });
            return config;
        }

        public static IConfigurationProvider AuthorMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BiblioData.DbModels.Book, Models.AuthorForm.BookForAuthor>();
                cfg.CreateMap<BiblioData.DbModels.MasterGroupAuthor, Models.AuthorForm.Author>()
                .ForMember(d => d.Books, o => o.Ignore())
                .ForMember(d => d.AuthorForBook, o => o.Ignore());
                cfg.CreateMap<BiblioData.DbModels.Author, Models.BookForm.AuthorForBook>();
            });
            return config;
        }

        public static IConfigurationProvider SearchMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BiblioData.DbModels.Author, Models.SearchForm.SearchAuthorItem>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.AuthorId))
                .ForMember(d => d.Description, o => o.MapFrom(s => s.FirstName + " " + s.LastName));
                cfg.CreateMap<BiblioData.DbModels.Book, Models.SearchForm.SearchBookItem>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.BookId))
                .ForMember(d => d.Description, o => o.MapFrom(s => s.Title));
                cfg.CreateMap<BiblioData.DbModels.BookContent, Models.SearchForm.SearchContentItem>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.BookId))
                .ForMember(d => d.Description, o => o.MapFrom(s => s.Title))
                .ForMember(d => d.Content, o => o.MapFrom(s => s.Content));                
            });
            return config;
        }

        public static IConfigurationProvider SettingMap()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Settings.SettingRecentDoc, ViewModels.DockWindows.Recent.RecentDoc>();
                cfg.CreateMap<ViewModels.DockWindows.Recent.RecentDoc, Settings.SettingRecentDoc>();
            });
            return config;
        }
    }

}
