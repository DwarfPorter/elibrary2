﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models
{
    public interface IAuthor
    {
        int AuthorId { get;  }
    }
}
