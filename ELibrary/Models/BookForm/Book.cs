﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models.BookForm
{
    public class Book: IBook
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public int? SequenceNumber { get; set; }
        public string SequenceName { get; set; }
        public string Annotation { get;  set; }
        public string GenreCode { get; set; }
        public string GenreDescription { get; set; }

        public ObservableCollection<AuthorForBook> Authors { get; set; }
        public ObservableCollection<ContentForBook> Contents { get; set; }

        public Book()
        {
            Authors = new ObservableCollection<AuthorForBook>();
            Contents = new ObservableCollection<ContentForBook>();
        }
    }
}
