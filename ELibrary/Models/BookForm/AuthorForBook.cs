﻿using ELibrary.Extension;

namespace ELibrary.Models.BookForm
{
    public class AuthorForBook : IAuthor
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public string FullName
        {
            get
            {
                return (FirstName.AddOneSpaceOrEmptyIfNull() + MiddleName.AddOneSpaceOrEmptyIfNull() + LastName ?? string.Empty).TrimEnd();
            }
        }
    }
}
