﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models.BookForm
{
    public class ContentForBook
    {
        public int ContentId { get; set; }
        public string Header { get; set; }
    }
}
