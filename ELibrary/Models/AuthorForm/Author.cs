﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Extension;
using ELibrary.Models.BookForm;

namespace ELibrary.Models.AuthorForm
{
    public class Author :IAuthor
    {
        public int MasterAuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public int AuthorId { get { return MasterAuthorId; } }

        public string FullName
        {
            get
            {
                return (FirstName.AddOneSpaceOrEmptyIfNull() + MiddleName.AddOneSpaceOrEmptyIfNull() + LastName ?? string.Empty).TrimEnd();
            }
        }

        public ObservableCollection<BookForAuthor> Books { get; set; }
        public ObservableCollection<AuthorForBook> AuthorForBook { get; set; }

        public Author()
        {
            Books = new ObservableCollection<BookForAuthor>();
            AuthorForBook = new ObservableCollection<AuthorForBook>();
        }
    }
}
