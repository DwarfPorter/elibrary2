﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models.AuthorForm
{
    public class BookForAuthor: IBook
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public int? SequenceNumber { get; set; }
        public string SequenceName { get; set; }
        public string GenreCode { get; set; }
        public string GenreDescription { get; set; }
    }
}
