﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Extension;

namespace ELibrary.Models.CatBookForm
{
    public class AuthorForCatBook : IAuthor
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return (FirstName.AddOneSpaceOrEmptyIfNull() + MiddleName.AddOneSpaceOrEmptyIfNull() + LastName ?? string.Empty).TrimEnd();
            }
        }

    }
}
