﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Extension;

namespace ELibrary.Models.CatBookForm
{
    public class CatBook: IBook
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public int? SequenceNumber { get; set; }
        public string SequenceName { get; set; }
        public string GenreCode { get; set; }
        public string GenreDescription { get; set; }

        public List<AuthorForCatBook> Authors { get; set; }

        private string _authorsFullNames;
        public string AuthorsFullName
        {
            get
            {
                if (_authorsFullNames == null)
                {
                    _authorsFullNames = Authors.Select(a => a.FullName).Aggregate((afn1, afn2) => afn1 +Symbols.GuiDelimiter + afn2);
                }
                return _authorsFullNames;
            }
        }

        public CatBook()
        {
            Authors = new List<AuthorForCatBook>();
            _authorsFullNames = null;
        }
    }
}
