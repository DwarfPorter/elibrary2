﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibrary.Models.SearchForm
{
    public class Search
    {
        public string SearchText { get; set; }
        public bool ByAuthor { get; set; }
        public bool ByTitle { get; set; }
        public bool ByContent { get; set; }

        public ObservableCollection<ISearchItem> SearchItems { get; set; }

        public SearchType SearchType
        {
            get
            {
                SearchType answer = SearchType.Nothing;
                if (ByAuthor) answer = answer | SearchType.Author;
                if (ByTitle) answer = answer | SearchType.Book;
                if (ByContent) answer = answer | SearchType.Content;
                return answer;
            }
        }

        public Search()
        {
            SearchItems = new ObservableCollection<ISearchItem>();
           
            ByTitle = true;
        }
    }
}
