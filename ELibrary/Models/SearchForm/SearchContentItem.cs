﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models.SearchForm
{
    public class SearchContentItem : SearchAbstractBookItem
    {
        public override string Type
        {
            get
            {
                return "Оглавление";
            }
        }
    }
    
}
