﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.Models.SearchForm
{
    public abstract class SearchItem : ISearchItem
    {
        public string Content { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public string SearchText { get; set; }

        public abstract string Type { get; }
        public abstract void OpenItem(Commands Commands);
    }
}
