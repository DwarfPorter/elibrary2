﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.Models.SearchForm
{
    public interface ISearchItem
    {
        int Id { get; set; }
        string Description { get; set; }
        string Type { get; }
        string Content { get; set; }
        string SearchText { get; set; }

        void OpenItem(Commands commands);
    }
}
