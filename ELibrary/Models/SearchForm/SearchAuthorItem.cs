﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.Models.SearchForm
{
    public class SearchAuthorItem : SearchItem
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public override string Type
        {
            get
            {
                return "Автор";
            }
        }

        public override void OpenItem(Commands commands)
        {
            commands.AuthorDetailCommand.Execute(new SearchAuthorDetail(this));
        }
    }

    internal class SearchAuthorDetail : IAuthor
    {
        private ISearchItem _author;
        public int AuthorId
        {
            get
            {
                return _author.Id;
            }
        }

        public SearchAuthorDetail(ISearchItem author)
        {
            _author = author;
        }
    }
}
