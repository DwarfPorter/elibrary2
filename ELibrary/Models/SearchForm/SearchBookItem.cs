﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.Models.SearchForm
{

    public abstract class SearchAbstractBookItem : SearchItem
    {

        public string Annotation { get; set; }

        public override void OpenItem(Commands commands)
        {
            commands.BookDetailCommand.Execute(new SearchBookDetail(this));
        }
    }

    public class SearchBookItem : SearchAbstractBookItem
    {

        public override string Type
        {
            get
            {
                return "Книга";
            }
        }

    }

    internal class SearchBookDetail : IBook
    {
        private ISearchItem _book;
        public int BookId
        {
            get
            {
                return _book.Id;
            }
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public SearchBookDetail(ISearchItem book)
        {
            _book = book;
        }

    }
}
