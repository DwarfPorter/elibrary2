﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Models.CatAuthorForm 
{
    public class CatAuthor : IAuthor
    {
        public int MasterAuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public int AuthorId { get { return MasterAuthorId; }  }
    }
}
