﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ELibrary.Views.DockForms.Catalogues
{
    /// <summary>
    /// Interaction logic for CatBooksForm.xaml
    /// </summary>
    public partial class CatBooksForm : UserControl
    {
        public CatBooksForm()
        {
            InitializeComponent();
        }

        private void MenuItem_OpenSite_Click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ViewModels.DockWindows.Catalogues.CatBooksDockWindowViewModel;
            context.Commands.OpenSiteBookCommand.Execute(context.SelectedBook.FileName);
        }

        private void MenuItem_BookDetail_Click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ViewModels.DockWindows.Catalogues.CatBooksDockWindowViewModel;
            context.Commands.BookDetailCommand.Execute(context.SelectedBook);
        }

        private void MenuItem_OpenBook_Click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ViewModels.DockWindows.Catalogues.CatBooksDockWindowViewModel;
            context.Commands.OpenBookCommand.Execute(context.SelectedBook.FileName);
        }
    }
}
