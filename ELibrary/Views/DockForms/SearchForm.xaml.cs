﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ELibrary.Views.DockForms
{
    /// <summary>
    /// Interaction logic for SearchForm.xaml
    /// </summary>
    public partial class SearchForm : UserControl
    {
        public SearchForm()
        {
            InitializeComponent();
            Keyboard.Focus(txtSearch);
            //txtSearch.SelectAll();
            
        }

        private void MenuItem_OpenDetail_Click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as ViewModels.DockWindows.Search.SearchWindowViewModel;
            context.OpenItemCommand.Execute(context.SelectedSearchItem);
        }
    }
}
