﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ELibrary.ViewModels;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Recent;
using ELibrary.ViewModels.DockWindows.References;


namespace ELibrary.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
            RecentList.MenuClick += (s, e) => OpenRecentDoc(e);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OpenRecentDoc(MenuClickRecentDocEventArgs recentDoc)
        {
            MainWindowViewModel dataContext = (MainWindowViewModel)DataContext;
            var commands = new Commands();
            dataContext.DockManagerViewModel.SetEvent_NewDocWindow(commands);
            if (recentDoc.Type == "ELibrary.ViewModels.DockWindows.Catalogues.AuthorDockWindowViewModel")
            {
                commands.AuthorDetailCommand.Execute(new WrapperAuthor(recentDoc));
            }
            else if (recentDoc.Type == "ELibrary.ViewModels.DockWindows.Catalogues.BookDockWindowViewModel")
            {
                commands.BookDetailCommand.Execute(new WrapperBook(recentDoc));
            }

            dataContext.DockManagerViewModel.ClearEvent_NewDocWindow(commands);
        }

    }
}
