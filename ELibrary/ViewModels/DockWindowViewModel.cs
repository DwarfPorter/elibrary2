﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.ViewModels
{
    public abstract class DockWindowViewModel : INotifyPropertyChanged
    {

        public DockWindowViewModel()
        {
            this.IsClosed = false;
            Commands = new Commands();
        }

        #region Properties
        #region Commands

        #region CloseCommand
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                    _CloseCommand = new RelayCommand(call => Close());
                return _CloseCommand;
            }
        }
        #endregion
        #endregion
        #region IsClosed
        private bool _IsClosed;
        public bool IsClosed
        {
            get { return _IsClosed; }
            set
            {
                if (_IsClosed != value)
                {
                    _IsClosed = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion
        #region Title
        private string _Title;
        public virtual string Title
        {
            get { return _Title; }
            set
            {
                if (_Title != value)
                {
                    _Title = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion
        #endregion

        public void Close()
        {
            this.IsClosed = true;
        }

        #region Commands

        public Commands Commands { get; set; }
        #endregion

        #region EventPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
