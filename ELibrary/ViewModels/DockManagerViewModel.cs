﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.ViewModels
{
    public class DockManagerViewModel : INotifyPropertyChanged, IDockManagerViewModel
    {
        #region Properties
        public ObservableCollection<DockWindowViewModel> Documents { get; set; }

        private DockWindowViewModel _activeDocument;
        public DockWindowViewModel ActiveDocument
        {
            get { return _activeDocument; }
            set
            {
                if (value == _activeDocument) return;
                _activeDocument = value;
                OnPropertyChanged();
            }
        }
        #endregion

        public DockManagerViewModel()
        {
            Documents = new ObservableCollection<DockWindowViewModel>();
        }

        public void AddDockWndow(DockWindowViewModel document)
        {
            document.PropertyChanged += Document_PropertyChanged;
            SetEvent_NewDocWindow(document.Commands);
            Documents.Add(document);
            ActiveDocument = document;
        }

        public void SetEvent_NewDocWindow(Commands commands)
        {
            commands.NewDockWindowRequested += Document_NewDockWindowRequested;
        }

        public void ClearEvent_NewDocWindow(Commands commands)
        {
            commands.NewDockWindowRequested -= Document_NewDockWindowRequested;
        }

        private void Document_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            DockWindowViewModel document = sender as DockWindowViewModel;

            if (e.PropertyName == nameof(DockWindowViewModel.IsClosed))
            {
                if (document.IsClosed)
                {
                    document.PropertyChanged -= Document_PropertyChanged;
                    ClearEvent_NewDocWindow(document.Commands);
                    this.Documents.Remove(document);
                }
            }
        }

        private void Document_NewDockWindowRequested(object sender, DockWindowEventArgs e)
        {
            Type typeDWindow = e.TypeDockWindowViewModel;
            int id = e.Id;
            var dwindow = (DockWindowViewModel)Activator.CreateInstance(typeDWindow, id);
            AddDockWndow(dwindow);
        }

        #region EventPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
