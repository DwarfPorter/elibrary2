﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows.Catalogues;
using Microsoft.Win32;
using ELibrary.Creator;

namespace ELibrary.ViewModels.DockWindows
{
    public class Commands
    {

        #region EventNewDockWindowRequested
        public event EventHandler<DockWindowEventArgs> NewDockWindowRequested;
        protected internal virtual void OnNewDockWindowRequested<T>(int id) where T : DockWindowViewModel
        {
            NewDockWindowRequested?.Invoke(this, new DockWindowEventArgs<T>(id));
        }
        #endregion

        #region BookDetailCommand
        private RelayCommand _bookDetailCommand;
        public RelayCommand BookDetailCommand
        {
            get
            {
                return _bookDetailCommand ?? (_bookDetailCommand = new RelayCommand((o) =>
                {
                    if (o == null) return;
                    OnNewDockWindowRequested<BookDockWindowViewModel>(((Models.IBook)o).BookId);
                }));
            }
        }
        #endregion

        #region OpenSiteBookCommand
        private RelayCommand _openSiteBookCommand;
        public RelayCommand OpenSiteBookCommand
        {
            get
            {
                return _openSiteBookCommand ?? (_openSiteBookCommand = new RelayCommand((o) =>
                {
                    if (o == null) return;
                    var pathfile = o as string;
                    if (!File.Exists(pathfile)) return;
                    Process.Start(new ProcessStartInfo("explorer.exe", " /select, \"" + pathfile + "\""));
                }));
            }
        }
        #endregion

        #region OpenBookCommand
        private RelayCommand _openBookCommand;
        public RelayCommand OpenBookCommand
        {
            get
            {
                return _openBookCommand ?? (_openBookCommand = new RelayCommand((o) =>
                {
                    if (o == null) return;
                    var pathfile = o as string;
                    if (!File.Exists(pathfile)) return;
                    if (!HasRegisteredFileExstension(Path.GetExtension(pathfile))) return;
                    Process.Start(pathfile);
                }));
            }
        }
        #endregion

        #region AuthorDetailCommand
        private RelayCommand _authorDetailCommand;
        public RelayCommand AuthorDetailCommand
        {
            get
            {
                return _authorDetailCommand ?? (_authorDetailCommand = new RelayCommand((o) =>
                {
                    if (o == null) return;
                    OnNewDockWindowRequested<AuthorDockWindowViewModel>(((Models.IAuthor)o).AuthorId);
                }));
            }
        }
        #endregion

        private bool HasRegisteredFileExstension(string fileExstension)
        {
            RegistryKey rkRoot = Registry.ClassesRoot;
            RegistryKey rkFileType = rkRoot.OpenSubKey(fileExstension);

            return rkFileType != null;
        }
    }
}
