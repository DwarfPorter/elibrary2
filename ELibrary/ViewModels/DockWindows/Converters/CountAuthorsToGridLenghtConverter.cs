﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using ELibrary.Models.BookForm;

namespace ELibrary.ViewModels.DockWindows.Converters
{
    public class CountAuthorsToGridLenghtConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IEnumerable<AuthorForBook> authors = value as IEnumerable<AuthorForBook>;
            if (authors == null || !authors.Any()) return new GridLength(0);
            return new GridLength(1, GridUnitType.Star);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GridLength val = (GridLength)value;
            return val.Value;
        }
    }
}
