﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.ViewModels.DockWindows.Search
{
    [Flags]
    public enum SearchType
    {
        Nothing = 0,
        Author = 1,
        Book = 2,
        Content = 4
    }
}
