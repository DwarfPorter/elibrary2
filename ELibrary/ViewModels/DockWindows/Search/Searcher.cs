﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BiblioData.Creator;
using ELibrary.Models.SearchForm;

namespace ELibrary.ViewModels.DockWindows.Search
{
    public class Searcher : ISearcher
    {
        private ICreator _creator;
        
        #region Constructors
        public Searcher() : this(new CreatorDefault())
        {

        }
        public Searcher(ICreator creator) : base()
        {
            _creator = creator;
        }

        #endregion

        public List<ISearchItem> Find(string searchText, SearchType searchType)
        {
            var answer = new List<ISearchItem>();
            if (searchType == SearchType.Nothing) return answer;
            
            using (var dbo = _creator.CreateDataOperation())
            {
                var cfgMapper = Mapper.InitMapper.SearchMap();
                var mapper = cfgMapper.CreateMapper();
                if (searchType.HasFlag(SearchType.Author))
                {
                    var dAuthor = dbo.SearchAuthorsByMask(searchText);
                    var tAuthor = dAuthor.ProjectToList<Models.SearchForm.SearchAuthorItem>(cfgMapper);
                    tAuthor.ForEach(a => { a.Content = a.FirstName != null && a.FirstName.Contains(searchText) ? a.FirstName : a.MiddleName != null && a.MiddleName.Contains(searchText) ? a.MiddleName : a.LastName != null && a.LastName.Contains(searchText) ?  a.LastName :  a.NickName; a.SearchText = searchText; });
                    answer.AddRange(tAuthor);
                }
                if (searchType.HasFlag(SearchType.Book))
                {
                    var dBook = dbo.SearchBooksByMask(searchText);
                    var tBook = dBook.ProjectToList<Models.SearchForm.SearchBookItem>(cfgMapper);
                    tBook.ForEach( b => { b.Content = b.Description != null && b.Description.Contains(searchText) ? b.Description : b.Annotation ; b.SearchText = searchText; });
                    answer.AddRange(tBook);
                }
                if (searchType.HasFlag(SearchType.Content))
                {
                    var dBookContent = dbo.SearchBooksByContent(searchText);
                    var tBookContent = dBookContent.ProjectToList<SearchContentItem>(cfgMapper);
                    tBookContent.ForEach(b => { b.SearchText = searchText; });
                    answer.AddRange(tBookContent);
                }
            }
            return answer;
        }
    }
}
