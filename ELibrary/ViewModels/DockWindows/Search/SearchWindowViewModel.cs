﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using ELibrary.Models.SearchForm;
using ELibrary.Settings;
using ELibrary.Views.DockForms;

namespace ELibrary.ViewModels.DockWindows.Search
{
    //
    // Two method of create child windows:
    // https://code.msdn.microsoft.com/windowsdesktop/WPF-Child-Window-with-MVVM-f8e32d3e
    // http://stackoverflow.com/questions/16652501/open-a-new-window-in-mvvm
    //
    public class SearchWindowViewModel : DockWindowViewModel
    {

        private ELibrary.Creator.ICreator _creator;
        /* public Commands Commands { get; set; }*/

        private IPersisterGuiSetting persisterGuiSetting = null;

        private ObservableCollection<string> _searchTexts;
        public ObservableCollection<string> SearchTexts
        {
            get { return _searchTexts; }
            set
            {
                if (_searchTexts != value)
                {
                    _searchTexts = value;
                    OnPropertyChanged();
                }
            }
        }

        #region Property Search
        private ELibrary.Models.SearchForm.Search _search;
        public ELibrary.Models.SearchForm.Search Search
        {
            get
            {
                return _search;
            }
            set
            {
                if (value == _search) return;
                _search = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Property SelectedSearchItem
        private ISearchItem _selectedSearchItem;
        public ISearchItem SelectedSearchItem
        {
            get
            {
                return _selectedSearchItem;
            }
            set
            {
                if (value == _selectedSearchItem) return;
                _selectedSearchItem = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors

        public SearchWindowViewModel():this(new ELibrary.Creator.CreatorDefault())
        {

        }

        public SearchWindowViewModel(ELibrary.Creator.ICreator creator)
        {
            _creator = creator;
            Search = new ELibrary.Models.SearchForm.Search();

            persisterGuiSetting = creator.CreatePersisterGuiSetting();
            var setting = persisterGuiSetting.Load();
            SearchTexts = new ObservableCollection<string>(setting.SearchTexts ?? Enumerable.Empty<string>());
        }
        #endregion

        #region Search
        private RelayCommand _searchCommand;
        public RelayCommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new RelayCommand((o) =>
                {
                    SearchTexts.Insert(0, Search.SearchText);
                    if (SearchTexts.Count() > GuiSetting.MaxSearchTexts) SearchTexts.RemoveAt(GuiSetting.MaxSearchTexts);
                    persisterGuiSetting.Save(s => s.SearchTexts = new List<string>(SearchTexts));
                    var searcher = _creator.CreateSearcher();
                    searcher.Find(Search.SearchText, Search.SearchType).ForEach(s => Search.SearchItems.Add(s));
                    //Search.SearchItems = new System.Collections.ObjectModel.ObservableCollection<ISearchItem>(searcher.Find(Search.SearchText, Search.SearchType));
                }
                ));
            }
        }
        #endregion

        private RelayCommand _openItemCommand;
        public RelayCommand OpenItemCommand
        {
            get
            {
                return _openItemCommand ?? (_openItemCommand = new RelayCommand((o) =>
                {
                    ISearchItem item = o as ISearchItem;
                    if (item == null) return;
                    item.OpenItem(Commands);
                }));
            }
        }

        private RelayCommand _clearCommand;
        public RelayCommand ClearCommand
        {
            get
            {
                return _clearCommand ?? (_clearCommand = new RelayCommand((o) =>
                {
                    Search.SearchItems.Clear();
                }));
            }
        }

        

    }


}
