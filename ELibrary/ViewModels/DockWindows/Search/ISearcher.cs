﻿using System.Collections.Generic;
using ELibrary.Models.SearchForm;

namespace ELibrary.ViewModels.DockWindows.Search
{
    public interface ISearcher
    {
        List<ISearchItem> Find(string searchText, SearchType searchType);
    }
}