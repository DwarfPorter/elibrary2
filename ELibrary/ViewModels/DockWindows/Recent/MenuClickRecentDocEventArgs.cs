﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Models;

namespace ELibrary.ViewModels.DockWindows.Recent
{
    public class MenuClickRecentDocEventArgs:EventArgs
    {
        public string Type { get; private set; }
        public int Id { get; private set; }

        public MenuClickRecentDocEventArgs(int id, string type)
        {
            Type = type;
            Id = id;
        }
    }
}
