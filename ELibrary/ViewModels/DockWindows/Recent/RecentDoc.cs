﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace ELibrary.ViewModels.DockWindows.Recent
{
    public class RecentDoc : Settings.SettingRecentDoc
    {
//        [XmlIgnore]
        public MenuItem MenuItem { get; set; }

        public RecentDoc(int number, int id, string type, string name): base(number, id, type, name)
        {
        }

    }
}
