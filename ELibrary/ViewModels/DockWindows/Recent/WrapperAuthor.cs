﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Models;

namespace ELibrary.ViewModels.DockWindows.Recent
{
    public class WrapperAuthor : IAuthor
    {
        private MenuClickRecentDocEventArgs _eventArg;
        public int AuthorId
        {
            get
            {
                return _eventArg.Id;
            }
        }

        public WrapperAuthor(MenuClickRecentDocEventArgs eventArg)
        {
            _eventArg = eventArg;
        }
    }
}
