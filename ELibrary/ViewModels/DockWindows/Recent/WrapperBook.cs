﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Models;

namespace ELibrary.ViewModels.DockWindows.Recent
{
    public class WrapperBook : IBook
    {
        private MenuClickRecentDocEventArgs _eventArg;

        public int BookId
        {
            get
            {
                return _eventArg.Id;
            }
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public WrapperBook(MenuClickRecentDocEventArgs eventArg)
        {
            _eventArg = eventArg;
        }
    }
}
