﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AutoMapper;
using ELibrary.Settings;
using ELibrary.Creator;

namespace ELibrary.ViewModels.DockWindows.Recent
{
    public class RecentList : Separator
    {
        private static IMapper _mapper;

        public MenuItem RecentMenu { get; private set; }

        public event EventHandler<MenuClickRecentDocEventArgs> MenuClick;

        private Separator _separator = null;
        private IPersisterGuiSetting persisterGuiSetting = null;

        private List<RecentDoc> _recentDocs = null;

        static RecentList()
        {
            _mapper = Mapper.InitMapper.SettingMap().CreateMapper();
        }

        public RecentList():this(new CreatorDefault())
        {


        }

        public RecentList(ICreator creator)
        {
            persisterGuiSetting = creator.CreatePersisterGuiSetting();
            this.Loaded += (s, e) => HookRecentMenu();
        }

        private void HookRecentMenu()
        {
            var parent = Parent as MenuItem;
            if (parent == null) throw new ApplicationException("Parent of RecentDoc should be a MenuItem");
            if (RecentMenu == parent) return;
            if (RecentMenu != null) RecentMenu.SubmenuOpened -= RecentMenu_SubMenuOpened;
            RecentMenu = parent;
            RecentMenu.SubmenuOpened += RecentMenu_SubMenuOpened;
        }

        private void RecentMenu_SubMenuOpened(object sender, RoutedEventArgs e)
        {
            ClearMenuItems();
            LoadMenuItems();
            SetMenuItems();
        }

        private void ClearMenuItems()
        {

            if (_recentDocs != null)
            {
                foreach (var rd in _recentDocs)
                {
                    if (rd.MenuItem != null) RecentMenu.Items.Remove(rd.MenuItem);
                }
            }
            if (_separator != null) RecentMenu.Items.Remove(_separator);
            _separator = null;
        }

        private void LoadMenuItems()
        {
            var setting = persisterGuiSetting.Load();
            _recentDocs = new List<RecentDoc>();
            _mapper.Map(setting.RecentDocs, _recentDocs);
        }

        private void SaveMenuItem()
        {
            persisterGuiSetting.Save(s => { s.RecentDocs = new List<SettingRecentDoc>(); _mapper.Map(_recentDocs, s.RecentDocs); });
        }

        private void SetMenuItems()
        {
            if (_recentDocs == null) return;
            if (!_recentDocs.Any()) return;
            int indexMenuItem = RecentMenu.Items.IndexOf(this);
            foreach (var rd in _recentDocs)
            {
                rd.MenuItem = new MenuItem { Header = rd.Name };
                rd.MenuItem.Click += MenuItem_Click;
                RecentMenu.Items.Insert(++indexMenuItem, rd.MenuItem);
            }

            _separator = new Separator();
            RecentMenu.Items.Insert(++indexMenuItem, _separator);

        }

        private void MenuItem_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;

            OnMenuClick(menuItem);
        }

        protected virtual void OnMenuClick(MenuItem menuItem)
        {
            var eventArg = FindAndCreateEventArg(menuItem);
            if (eventArg == null) return;
            MenuClick?.Invoke(menuItem, eventArg);
        }

        private MenuClickRecentDocEventArgs FindAndCreateEventArg(MenuItem menuItem)
        {
            var rd = _recentDocs.FirstOrDefault(r => r.MenuItem == menuItem);
            if (rd == null) return null;
            return new MenuClickRecentDocEventArgs(rd.Id, rd.Type);
        }

    }
}
