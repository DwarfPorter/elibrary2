﻿using ELibrary.Creator;
using ELibrary.Models.AuthorForm;
using ELibrary.Mapper;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.Win32;

namespace ELibrary.ViewModels.DockWindows.Catalogues
{
    public class AuthorDockWindowViewModel : DockWindowViewModel
    {
        #region Property Author
        private Author _author;
        public Author Author
        {
            get
            {
                return _author;
            }
            set
            {
                if (value == _author) return;
                _author = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Property SelectedBook
        private BookForAuthor _selectedBook;
        public BookForAuthor SelectedBook
        {
            get
            {
                return _selectedBook;
            }
            set
            {
                if (value == _selectedBook) return;
                _selectedBook = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public AuthorDockWindowViewModel(int masterAuthorId):this(new CreatorDefault(), masterAuthorId)
        {

        }

        public AuthorDockWindowViewModel(ICreator creator, int masterAuthorId) : base()
        {
            using (var dbo = creator.CreateDataOperation())
            {
                var dAuthor = dbo.GetMasterGroupAuthor(masterAuthorId);
                var cfgMapper = InitMapper.AuthorMap();
                var mapper = cfgMapper.CreateMapper();
                Author = mapper.Map<Author>(dAuthor);
                var dBooks = dbo.GetBooksByMasterAuthor(masterAuthorId);
                List<BookForAuthor> tBooks = new List<BookForAuthor>();
                mapper.Map(dBooks, Author.Books);
                var tAuthor = dbo.GetAuthorsByMasterAuthor(masterAuthorId).ProjectToList<Models.BookForm.AuthorForBook>(cfgMapper);
                Author.AuthorForBook = new System.Collections.ObjectModel.ObservableCollection<Models.BookForm.AuthorForBook>(tAuthor);

                Title = "Автор '" + Author.FullName + "'";
            }

            var persisterSetting = creator.CreatePersisterGuiSetting();
            persisterSetting.Save(s => s.InsertRecentDoc(masterAuthorId, this.GetType(), Title));

        }
        #endregion

        private bool HasRegisteredFileExstension(string fileExstension)
        {
            RegistryKey rkRoot = Registry.ClassesRoot;
            RegistryKey rkFileType = rkRoot.OpenSubKey(fileExstension);

            return rkFileType != null;
        }
    }
}
