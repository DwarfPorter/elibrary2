﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using ELibrary.Creator;
using BiblioData.DbModels;
using BiblioSearchFB2.Parser;
using ELibrary.Extension;
using ELibrary.Mapper;
using ELibrary.Models.BookForm;
using System.Collections.Generic;

namespace ELibrary.ViewModels.DockWindows.Catalogues
{
    public class BookDockWindowViewModel : DockWindowViewModel
    {
        private ICreator _creator;

        #region Property Book
        private Models.BookForm.Book _book;
        public Models.BookForm.Book Book
        {
            get
            {
                return _book;
            }
            set
            {
                if (value == _book) return;
                _book = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region SelectedAuthor
        private AuthorForBook _selectedAuthor;
        public AuthorForBook SelectedAuthor
        {
            get
            {
                return _selectedAuthor;
            }
            set
            {
                if (value == _selectedAuthor) return;
                _selectedAuthor = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region BookCover
        private BitmapImage _bookCover;
        public BitmapImage BookCover
        {
            get
            {
                return _bookCover;
            }
            set
            {
                if (value == _bookCover) return;
                _bookCover = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region AuthorDetail by not master
        private RelayCommand _authorDetailCommand;
        public RelayCommand AuthorDetailCommand
        {
            get
            {
                return _authorDetailCommand ?? (_authorDetailCommand = new RelayCommand((o) =>
                {
                    if (o == null) return;
                    int masterAuthorId;
                    using (var dbo = _creator.CreateDataOperation())
                    {
                        masterAuthorId = dbo.GetMasterAuthorIdByAuthor(((Models.BookForm.AuthorForBook)o).AuthorId);
                    }
                    Commands.OnNewDockWindowRequested<AuthorDockWindowViewModel>(masterAuthorId);
                }
                ));
            }
        }
        #endregion

        #region Constructors
        public BookDockWindowViewModel(int bookId) : this(new CreatorDefault(), bookId)
        {

        }

        public BookDockWindowViewModel(ICreator creator, int bookId) : base()
        {
            _creator = creator;
            using (var dbo = _creator.CreateDataOperation())
            {
                var dBook = dbo.GetBook(bookId);
                var mapper = InitMapper.BookMap().CreateMapper();
                Book = mapper.Map<Models.BookForm.Book>(dBook);

                Title = "Книга '" + (Book.Title.Length < 10 ? Book.Title : Book.Title.Substring(0, 10) + Symbols.Ellipsis) + "'";

            }

            var persisterSetting = _creator.CreatePersisterGuiSetting();
            persisterSetting.Save(s => s.InsertRecentDoc(bookId, this.GetType(), Title));

            SetFaceBook();
       
        }
        #endregion

        private void SetFaceBook()
        {
            if (Book.FileName != null && File.Exists(Book.FileName))
            {
                Bitmap bitmap = ParserFB2.GetCover(Book.FileName);
                if (bitmap != null)
                {
                    using (MemoryStream memory = new MemoryStream())
                    {
                        bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                        memory.Position = 0;
                        BitmapImage bitmapimage = new BitmapImage();
                        bitmapimage.BeginInit();
                        bitmapimage.StreamSource = memory;
                        bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapimage.EndInit();
                        BookCover = bitmapimage;
                    }
                }
            }
        }

      /* private bool HasRegisteredFileExstension(string fileExstension)
        {
            RegistryKey rkRoot = Registry.ClassesRoot;
            RegistryKey rkFileType = rkRoot.OpenSubKey(fileExstension);

            return rkFileType != null;
        }*/
    }
}
