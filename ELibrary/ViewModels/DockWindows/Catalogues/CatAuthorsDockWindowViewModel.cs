﻿using System.Collections.ObjectModel;
using AutoMapper;
using BiblioData.Creator;
using ELibrary.Mapper;
using ELibrary.Models.CatAuthorForm;

namespace ELibrary.ViewModels.DockWindows.Catalogues
{

    public class CatAuthorsDockWindowViewModel : DockWindowViewModel
    {

        #region Properties
        public ObservableCollection<CatAuthor> Authors { get; set; }

        private CatAuthor _selectedAuthor;
        public CatAuthor SelectedAuthor
        {
            get
            {
                return _selectedAuthor;
            }
            set
            {
                if (value == _selectedAuthor) return;
                _selectedAuthor = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public CatAuthorsDockWindowViewModel():this(new CreatorDefault())
        {

        }
        public CatAuthorsDockWindowViewModel(ICreator creator):base()
        {
            using (var dbo = creator.CreateDataOperation())
            {
                var dAuthors = dbo.GetMasterGroupAuthors();
                var tAuthors = dAuthors.ProjectToList<CatAuthor>(InitMapper.CatAuthorMap());
                Authors = new ObservableCollection<CatAuthor>(tAuthors);
            }

        }
        #endregion
    }
}
