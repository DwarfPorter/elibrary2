﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using AutoMapper;
using BiblioData.Creator;
using ELibrary.Mapper;
using ELibrary.Models.CatBookForm;

namespace ELibrary.ViewModels.DockWindows.Catalogues
{
    public class CatBooksDockWindowViewModel : DockWindowViewModel
    {
        #region Properties
        public ObservableCollection<CatBook> Books { get; set; }

        private CatBook _selectedBook;
        public CatBook SelectedBook
        {
            get
            {
                return _selectedBook;
            }
            set
            {
                if (value == _selectedBook) return;
                _selectedBook = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public CatBooksDockWindowViewModel() : this(new CreatorDefault())
        {
        }

        public CatBooksDockWindowViewModel(ICreator creator)
        {
            using(var dbo = creator.CreateDataOperation())
            {
                var dBooks = dbo.GetBooks();
                var tBooks = dBooks.ProjectToList<CatBook>(InitMapper.CatBookMap());
                Books = new ObservableCollection<CatBook>(tBooks);
            }
            
        }
        #endregion


    }
}
