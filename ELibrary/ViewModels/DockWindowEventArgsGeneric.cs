﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.ViewModels
{
    public class DockWindowEventArgs<T>: DockWindowEventArgs 
        where T : DockWindowViewModel
    {
        public DockWindowEventArgs(int id):base(typeof(T), id) { }
    }
}
