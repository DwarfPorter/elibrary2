﻿using System;

namespace ELibrary.ViewModels
{
    public class RelayCommand<T> : RelayCommand
    {
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null) : base(Convert<T>(execute), Convert<T>(canExecute))
        {
        }

        public static Action<object> Convert<T1>(Action<T1> execute)
        {
            if (execute == null) return null;
            else return new Action<object>(o => execute((T1)o));
        }

        public static Func<object, bool> Convert<T1>(Func<T1, bool> canexecute)
        {
            if (canexecute == null) return null;
            else return new Func<object, bool>(o => canexecute((T1)o));
        }


    }
}
