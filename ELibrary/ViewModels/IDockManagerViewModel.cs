﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ELibrary.ViewModels.DockWindows;

namespace ELibrary.ViewModels
{
    public interface IDockManagerViewModel
    {
        DockWindowViewModel ActiveDocument { get; set; }
        ObservableCollection<DockWindowViewModel> Documents { get; set; }
        event PropertyChangedEventHandler PropertyChanged;
        void AddDockWndow(DockWindowViewModel document);
        void SetEvent_NewDocWindow(Commands commands);
        void ClearEvent_NewDocWindow(Commands commands);
    }
}