﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ELibrary.Creator;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Catalogues;
using ELibrary.ViewModels.DockWindows.References;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibrary.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {

        public IDockManagerViewModel DockManagerViewModel { get; private set; }

        private ICreator _creator;

        #region constructor
        public MainWindowViewModel() : this(new CreatorDefault())
        {
        }

        public MainWindowViewModel(ICreator creator)
        {
            _creator = creator;
            DockManagerViewModel = _creator.CreateDockManagerViewModel();
        }
        #endregion

        #region commands for main menu
        private RelayCommand _addNewCatBooksFormCommand;
        public RelayCommand AddNewCatBooksFormCommand
        {
            get
            {
                return _addNewCatBooksFormCommand ?? (_addNewCatBooksFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new CatBooksDockWindowViewModel() { Title = "Картотека по названиям" });

                }));
            }
        }

        private RelayCommand _addNewCatAuthorsFormCommand;
        public RelayCommand AddNewCatAuthorsFormCommand
        {
            get
            {
                return _addNewCatAuthorsFormCommand ?? (_addNewCatAuthorsFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new CatAuthorsDockWindowViewModel() { Title = "Картотека по авторам" });
                }));
            }
        }

        private RelayCommand _addNewRefAuthorsFormCommand;
        public RelayCommand AddNewRefAuthorsFormCommand
        {
            get
            {
                return _addNewRefAuthorsFormCommand ?? (_addNewRefAuthorsFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new RefAuthorsDockWindowViewModel() { Title = "Справочник по авторам" });
                }));
            }
        }

        private RelayCommand _addNewRefGenresFormCommand;
        public RelayCommand AddNewRefGenresFormCommand
        {
            get
            {
                return _addNewRefGenresFormCommand ?? (_addNewRefGenresFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new RefGenresDockWindowViewModel() { Title = "Справочник по жанрам" });
                }));
            }
        }

        private RelayCommand _addNewRefSequencesFormCommand;
        public RelayCommand AddNewRefSequencesFormCommand
        {
            get
            {
                return _addNewRefSequencesFormCommand ?? (_addNewRefSequencesFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new RefSequencesDockWindowViewModel() { Title = "Справочник по сериям" });
                }));
            }
        }

        private RelayCommand _addNewRefSequenceFormCommand;
        public RelayCommand AddNewRefSequenceFormCommand
        {
            get
            {
                return _addNewRefSequenceFormCommand ?? (_addNewRefSequenceFormCommand = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new RefSequenceDockWindowViewModel((int)o) { Title = "Настройка серии" });
                }));
            }
        }

        #endregion

        #region command for toolbar
        private RelayCommand _searchBook;
        public RelayCommand SearchBook
        {
            get
            {
                return _searchBook ?? (_searchBook = new RelayCommand((o) =>
                {
                    DockManagerViewModel.AddDockWndow(new SearchWindowViewModel() { Title = "Поиск" });
                }));
            }
        }
        #endregion

        #region EventPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
