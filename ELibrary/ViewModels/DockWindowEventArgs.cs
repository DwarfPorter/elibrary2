﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.ViewModels
{
    public class DockWindowEventArgs: EventArgs
    {

        public int Id { get; private set; }
        public Type TypeDockWindowViewModel { get; private set; }

        public DockWindowEventArgs(Type typeDockWindowViewModel, int id)
        {
            Id = id;
            TypeDockWindowViewModel = typeDockWindowViewModel;
        }

    }
}
