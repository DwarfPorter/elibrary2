﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Settings
{
    public interface ISettings
    {
        string FileNameGuiSetting { get; }
    }
}
