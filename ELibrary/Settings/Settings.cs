﻿using System;
using System.Configuration;

namespace ELibrary.Settings
{
    public class Settings : ISettings
    {
        private const string _fileNameGuiSetting = "FileNameGuiSetting";

        private const string _fileNameGuiSettingDefault = "GuiSetting.xml";

        public string FileNameGuiSetting
        {
            get
            {
                try
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string result = appSettings[_fileNameGuiSetting];
                    if (result == null) return _fileNameGuiSettingDefault;
                    return result;
                   

                }
                catch (ConfigurationErrorsException)
                {
                    return _fileNameGuiSettingDefault;
                }
            }
        }
        
    }
}
