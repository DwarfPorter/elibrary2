﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Settings
{
    [Serializable]
    public class GuiSetting
    {
        public static int MaxRecentDocs { get; set; }
        public List<SettingRecentDoc> RecentDocs { get; set; }
        public static int MaxSearchTexts { get; set; }
        public List<string> SearchTexts { get; set; }

        static GuiSetting()
        {
            MaxRecentDocs = 9;
            MaxSearchTexts = 10;
        }

        public void InsertRecentDoc(int id, Type type, string description)
        {
            if (RecentDocs == null) RecentDocs = new List<SettingRecentDoc>();
            var sType = type.FullName;
            if (RecentDocs.Any(d => d.Id == id && d.Type == sType)) return;
            RecentDocs.Insert(0, new SettingRecentDoc(0, id, sType, description));
            if (RecentDocs.Count() > MaxRecentDocs) RecentDocs.RemoveAt(MaxRecentDocs);
        }
    }
}
