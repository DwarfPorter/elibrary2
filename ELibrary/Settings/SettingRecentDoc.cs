﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Settings
{
    [Serializable]
    public class SettingRecentDoc
    {
        public string Type { get; set; }
        public int Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }

        public SettingRecentDoc(int number, int id, string type, string name)
        {
            Type = type;
            Number = number;
            Id = id;
            Name = name;
        }

        // For serialize
        public SettingRecentDoc() { }
    }
}
