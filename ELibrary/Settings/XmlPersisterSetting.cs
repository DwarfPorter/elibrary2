﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ELibrary.Settings
{
    public class XmlPersisterSetting : IPersisterGuiSetting
    {
        private string _fileNameGuiSetting;
        private XmlSerializer xmlserializer;
        private object _lock = new object();

        public XmlPersisterSetting() : this(new Settings())
        {

        }

        public XmlPersisterSetting(ISettings settings)
        {
            _fileNameGuiSetting = settings.FileNameGuiSetting;
            xmlserializer = new XmlSerializer(typeof(GuiSetting));
        }

        public GuiSetting Load()
        {
            lock (_lock)
            {
                return LoadFile();
            }
        }

        public void Save(GuiSetting setting)
        {
            lock (_lock)
            {
                File.WriteAllText(_fileNameGuiSetting, SaveCore(setting));
            }
        }

        public void Save(Action<GuiSetting> updateGuiSetting)
        {
            lock (_lock)
            {
                GuiSetting setting = LoadFile();
                updateGuiSetting(setting);
                File.WriteAllText(_fileNameGuiSetting, SaveCore(setting));
            }
        }

        private GuiSetting LoadFile()
        {
            if (File.Exists(_fileNameGuiSetting))
                return LoadCore(File.ReadAllText(_fileNameGuiSetting));
            return new GuiSetting();
        }

        internal string SaveCore(GuiSetting setting)
        {

            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings { /*OmitXmlDeclaration = true,*/ Indent = true, IndentChars = " " }))
            {
                xmlserializer.Serialize(writer, setting);

            }
            var answer = stringWriter.ToString();
            return answer;
        }

        internal GuiSetting LoadCore(string stringXML)
        {
            GuiSetting answer;

            using (TextReader reader = new StringReader(stringXML))
            {
                answer = (GuiSetting)xmlserializer.Deserialize(reader);
            }
            return answer;
        }
    }
}
