﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Settings
{
    public interface IPersisterGuiSetting
    {
        GuiSetting Load();
        void Save(GuiSetting setting);
        void Save(Action<GuiSetting> updateGuiSetting);
    }
}
