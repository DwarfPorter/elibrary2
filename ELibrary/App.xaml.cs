﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shell;
using ELibrary.Mapper;

namespace ELibrary
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());

            JumpTask task = new JumpTask
            {
                Title = "Recent Docs",
                Description = "My Docs",
                CustomCategory = "Action"
            };

            JumpList jumpList = new JumpList();
            jumpList.JumpItems.Add(task);

            JumpList.SetJumpList(Application.Current, jumpList);
            //InitMapper.RegisterMap();
        }
    }
}
