﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Settings;
using ELibrary.ViewModels;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibrary.Creator
{
    public interface ICreator : BiblioData.Creator.ICreator
    {
        IDockManagerViewModel CreateDockManagerViewModel();
        ISearcher CreateSearcher();
        IPersisterGuiSetting CreatePersisterGuiSetting();
    }
}
