﻿using BiblioData.Context;
using ELibrary.Settings;
using ELibrary.ViewModels;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibrary.Creator
{
    public class CreatorDefault : ICreator
    {
        public IDockManagerViewModel CreateDockManagerViewModel()
        {
            return new DockManagerViewModel();
        }

        public IPersisterGuiSetting CreatePersisterGuiSetting()
        {
            return new XmlPersisterSetting();
        }

        public ISearcher CreateSearcher()
        {
            return new Searcher();
        }

        public IDataOperation CreateDataOperation()
        {
            return (new BiblioData.Creator.CreatorDefault()).CreateDataOperation();
        }

    }
}
