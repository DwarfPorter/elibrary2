﻿using System;
using System.Configuration;

namespace BiblioService
{
    public class Settings : ISettings
    {
        private const string _executeTime = "ExecuteTime";
        private const string _bookCatalogue = "BookCatalogue";
        private const string _timerFileSearch = "TimerFileSearch";

        public TimeSpan? ExecuteTime
        {
            get
            {
                try
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string result = appSettings[_executeTime];
                    if (result == null) return null;

                    int hour = 0;
                    int minute = 0;

                    var answer = result.Split(':');
                    if (answer.Length >= 1) int.TryParse(answer[0], out hour);
                    if (answer.Length >= 2) int.TryParse(answer[1], out minute);

                    return new TimeSpan(hour, minute, 0);

                }
                catch (ConfigurationErrorsException)
                {
                    return null;
                }
            }
        }

        public bool TimerFileSearch
        {
            get
            {
                try
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string result = appSettings[_timerFileSearch];
                    if (result == null) return false;
                    bool answer;
                    if (!bool.TryParse(result, out answer)) {
                        if (result == "On") answer = true;
                        else answer = false;
                    } 
                    return answer;
                }
                catch (ConfigurationErrorsException)
                {
                    return false;
                }
            }
        }

        public string BookCatalogue
        {
            get
            {
                try
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string result = appSettings[_bookCatalogue];
                    if (result == null) return string.Empty;
                    return result;
                }
                catch (ConfigurationErrorsException)
                {
                    return string.Empty;
                }
            }
        }
    }
}
