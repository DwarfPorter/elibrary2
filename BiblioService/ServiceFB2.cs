﻿using BiblioLogger;
using BiblioSearchFB2.Searcher;
using System;
using System.IO;
using System.Security.Permissions;
using System.Timers;
using System.Threading.Tasks;
using BiblioService.Creator;

namespace BiblioService
{
    public class ServiceFB2
    {
        private IBiblioLog _biblioLogger;
        private Timer _timer = null;
        private static volatile bool _running = false;
        private object _lockObject = new object();
        private FileSystemWatcher _watcher;
        private bool _isDbExists = false;
        private ISettings _setting;
        private ICreator _creator;

        public ServiceFB2(bool runAsPrompt) : this(runAsPrompt, new CreateDefault())
        {
        }

        public ServiceFB2(bool runAsPrompt, ICreator creator)
        {
            _creator = creator;
            Init(runAsPrompt);
        }

        private void Init(bool runAsPrompt)
        {
            _biblioLogger = _creator.CreateBiblioLog();
            _setting = _creator.CreateSettings();
            using (var db = _creator.CreateDataOperation())
            {
                _isDbExists = db.ExistsDb();
            }
            if (runAsPrompt)
            {
                if (!_isDbExists)
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.Execute(_setting.BookCatalogue);
                }
                //else

                //CreateFileWatcher();

            }
            else
            {
                _biblioLogger.Info("BiblioService is run as service");
                CreateFileWatcher();
                if (!_isDbExists)
                {
                    Task.Run(() =>
                    {
                        _biblioLogger.Info("BiblioService is processing");
                        ISearchController searcher = _creator.CreateSearchController();
                        searcher.Execute(_setting.BookCatalogue);
                    });
                }
                if (_setting.TimerFileSearch)
                {
                    double interval = 60 * 1000;
                    _timer = new Timer(interval);
                    _timer.Elapsed += new ElapsedEventHandler(OnTick);
                }
            }
        }


        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private void CreateFileWatcher()
        {
            _watcher = new FileSystemWatcher(_setting.BookCatalogue, "*.fb2");
            _watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
            _watcher.IncludeSubdirectories = true;
            _watcher.Changed += new FileSystemEventHandler(OnChanged);
            _watcher.Created += new FileSystemEventHandler(OnChanged);
            _watcher.Deleted += new FileSystemEventHandler(OnChanged);
            _watcher.Renamed += new RenamedEventHandler(OnRenamed);
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private void SetFileWatcherHandlers()
        {
            _watcher.EnableRaisingEvents = true;
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private void ClearFileWatcherHandlers()
        {
            _watcher.EnableRaisingEvents = false;
        }

        // Define the event handlers.
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            OnChangedCore(e);
        }

        internal void OnChangedCore(FileSystemEventArgs e)
        {
            try
            {
                if (e.ChangeType.HasFlag(WatcherChangeTypes.Deleted))
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.ProcessFileIfRemoved(e.FullPath);
                }
                else if (e.ChangeType.HasFlag(WatcherChangeTypes.Created))
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.ProcessFile(e.FullPath);
                }
            }
            catch (Exception ex)
            {
                _biblioLogger.Fatal(string.Format("Exception occurs on processing [{0}]:", e.FullPath), ex);
            }

        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {

            OnRenamedCore(e);
        }

        internal void OnRenamedCore(RenamedEventArgs e)
        {
            try
            {
                if (Path.GetExtension(e.OldFullPath) == ".fb2" && Path.GetExtension(e.FullPath) != ".fb2")
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.ProcessFileIfRemoved(e.OldFullPath);
                }
                else if (Path.GetExtension(e.OldFullPath) != ".fb2" && Path.GetExtension(e.FullPath) == ".fb2")
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.ProcessFile(e.FullPath);
                }
                else if (Path.GetExtension(e.OldFullPath) == ".fb2" && Path.GetExtension(e.FullPath) == ".fb2")
                {
                    ISearchController searcher = _creator.CreateSearchController();
                    searcher.ProcessFileIfRenamed(e.OldFullPath, e.FullPath);
                }
            }
            catch (Exception ex)
            {
                _biblioLogger.Fatal(string.Format("Exception occurs on renaming [{0} => {1}]:", e.OldFullPath, e.FullPath), ex);
            }
        }

        protected virtual void OnTick(object sender, ElapsedEventArgs e)
        {
            _biblioLogger.Info("BiblioService on tick");
            if (!_setting.TimerFileSearch) return;
            if (!_running)
            {
                lock (_lockObject)
                {
                    if (!_running)
                    {
                        _running = true;

                        var timeExecute = _setting.ExecuteTime;
                        bool isProceed = false;
                        if (timeExecute == null)
                        {
                            timeExecute = new TimeSpan(4, 0, 0);
                        }

                        var now = DateTime.Now;
                        if (timeExecute.Value.Hours == now.Hour && timeExecute.Value.Minutes == now.Minute)
                        {
                            isProceed = true;
                        }

                        if (isProceed)
                        {
                            _biblioLogger.Info("BiblioService is processing");
                            ISearchController searcher = _creator.CreateSearchController();
                            searcher.Execute(_setting.BookCatalogue);
                        }
                        _running = false;
                    }
                }
            }

        }

        public void Start()
        {
            _biblioLogger.Info("BiblioService is Started");

            if (_watcher != null)
            {
                SetFileWatcherHandlers();
            }
            if (_timer != null)
            {
                _timer.AutoReset = true;
                _timer.Enabled = true;
                _timer.Start();
            }
        }

        public void Stop()
        {
            _biblioLogger.Info("BiblioService is Stopped");

            if (_watcher != null)
            {
                ClearFileWatcherHandlers();
            }
            if (_timer != null)
            {
                _timer.AutoReset = false;
                _timer.Enabled = false;
            }
        }

    }
}
