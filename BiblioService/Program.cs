﻿using BiblioLogger;
using Topshelf;

namespace BiblioService
{
    class Program
    {

        private static IBiblioLog _biblioLog = (new BiblioLogger.Creator.CreatorDefault()).CreateBiblioLog();

        static void Main(string[] args)
        {
            _biblioLog.Info("BiblioService is running");

            HostFactory.Run(x =>
            {

                _biblioLog.Info("BiblioService is setting");

                var RunAsPrompt = false;

                if (args.Length == 0)
                {
                    RunAsPrompt = true;
                }

                x.Service<ServiceFB2>(s =>
                {
                    _biblioLog.Info("BiblioService is creating");
                    s.ConstructUsing(name => new ServiceFB2(RunAsPrompt));
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });


                if (RunAsPrompt)
                {
                    x.RunAsPrompt();
                }
                else
                {
                    x.RunAsLocalSystem();
                }

                x.SetServiceName("BiblioService");
                x.SetDescription("BiblioService Collect fb2 files to database");
                x.SetDisplayName("BiblioService");
                x.SetServiceName("BiblioService");

            });

        }
    }
}
