﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiblioData.Context;
using BiblioLogger;
using BiblioSearchFB2.Searcher;

namespace BiblioService.Creator
{
    public class CreateDefault : ICreator
    {
        public IBiblioLog CreateBiblioLog()
        {
            return (new BiblioSearchFB2.Creator.CreatorDefault()).CreateBiblioLog();
        }

        public IDataOperation CreateDataOperation()
        {
            return (new BiblioSearchFB2.Creator.CreatorDefault()).CreateDataOperation();
        }

        public IDirOperation CreateDirOperation()
        {
            return (new BiblioSearchFB2.Creator.CreatorDefault()).CreateDirOperation();
        }

        public ISearchController CreateSearchController()
        {
            return (new BiblioSearchFB2.Creator.CreatorDefault()).CreateSearchController();
        }

        public ISettings CreateSettings()
        {
            return new Settings();
        }
    }
}
