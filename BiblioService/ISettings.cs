﻿using System;

namespace BiblioService
{
    public interface ISettings
    {
        string BookCatalogue { get; }
        TimeSpan? ExecuteTime { get; }
        bool TimerFileSearch { get; }
    }
}