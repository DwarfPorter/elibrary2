﻿using BiblioData.Context;

namespace BiblioData.Creator
{
    public interface ICreator
    {
        IDataOperation CreateDataOperation();
    }
}
