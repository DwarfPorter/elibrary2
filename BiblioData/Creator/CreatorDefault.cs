﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiblioData.Context;

namespace BiblioData.Creator
{
    public class CreatorDefault : ICreator
    {
        public IDataOperation CreateDataOperation()
        {
            return new DataOperation();
        }
    }
}
