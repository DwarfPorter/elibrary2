﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.DbModels
{
    public class MasterAuthor
    {
        public int AuthorId { get; set; }
        public int MasterAuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public ICollection<Book> Books { get; set; }

        public MasterAuthor()
        {
            Books = new List<Book>();
        }
    }
}
