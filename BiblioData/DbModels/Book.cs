﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.DbModels
{
    public class Book
    {
        public int BookId { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        [StringLength(500)]
        public string FileName { get; set; }
        public string MD5 { get; set; }
        public int? SequenceNumber { get; set; }
        public string MachineName { get; set; }
        [StringLength(1000)]
        public string Annotation { get; set; }
        public string KeyWords { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Author> Authors { get; set; }
        public int GenreId { get; set; }
        public virtual Genre Genre { get; set; }
        public int? SequenceId { get; set; }
        public virtual Sequence Sequence { get; set; }
        public ICollection<Content> Contents { get; set; }

        public Book()
        {
            Authors = new List<Author>();
            Contents = new List<Content>();
        }
    }
}
