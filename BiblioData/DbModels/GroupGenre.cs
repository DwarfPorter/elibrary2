﻿using System.Collections.Generic;

namespace BiblioData.DbModels
{
    public class GroupGenre
    {
        public int GroupGenreId { get; set; }
        public string Description { get; set; }
        public ICollection<Genre> Genres { get; set; }

        public GroupGenre()
        {
            Genres = new List<Genre>();
        }
    }
}