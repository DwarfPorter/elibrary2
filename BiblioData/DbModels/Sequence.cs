﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.DbModels
{
    public class Sequence
    {
        public int SequenceId { get; set; }
        public string Name { get; set; }

        public virtual Sequence MasterSequence { get; set; }
        public ICollection<Book> Books {get;set;}

        public Sequence()
        {
            Books = new List<Book>();
        }
    }
}
