﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.DbModels
{
    public class Content
    {
        public int ContentId { get; set; }
        public int BookId { get; set; }
        public string Header { get; set; }
    }
}
