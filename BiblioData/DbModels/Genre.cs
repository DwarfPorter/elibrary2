﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.DbModels
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }

        public int? GroupGenreId { get; set; }
        public virtual GroupGenre GroupGenre {get;set;}
        public ICollection<Book> Books { get; set; }

        public Genre()
        {
            Books = new List<Book>();
        }
    }
}
