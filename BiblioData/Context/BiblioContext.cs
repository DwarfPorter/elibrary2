﻿using BiblioData.DbModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.Context
{
    public class BiblioContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Sequence> Sequences { get; set; }
        public DbSet<GroupGenre> GroupGenres { get; set; }
        public DbSet<Content> Contents { get; set; }

        public BiblioContext() : base("BiblioConnection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                .HasMany(a => a.Books)
                .WithMany(b => b.Authors)
                .Map(t => t.MapLeftKey("AuthorId")
                    .MapRightKey("BookId")
                    .ToTable("AuthorBook"));

            base.OnModelCreating(modelBuilder);
        }
    }
}

// Need new table MasterGenres with main genre