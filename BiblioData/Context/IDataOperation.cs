﻿using BiblioData.DbModels;
using System;
using System.Linq;

namespace BiblioData.Context
{
    public interface IDataOperation : IDisposable
    {
        bool ExistsDb();
        Author SearchAuthor(string firstName, string lastName = null);
        Book SearchBookByMD5(string md5);
        Book SearchBookByFileName(string fileName);
        Genre SearchGenre(string name);
        Sequence SearchSequence(string name);
        void AddAuthor(Author author);
        void AddBook(Book book);
        void AddGenre(Genre genre);
        void AddSequence(Sequence sequence);
        void AddContent(Content content);
        IQueryable<Book> GetBooks();
        IQueryable<Author> GetAuthors();
        IQueryable<MasterAuthor> GetMasterAuthors();
        IQueryable<MasterGroupAuthor> GetMasterGroupAuthors();
        Book GetBook(int bookId);
        MasterGroupAuthor GetMasterGroupAuthor(int masterAuthorId);
        IQueryable<MasterAuthor> GetMasterAuthor(int authorId);
        IQueryable<Book> GetBooksByMasterAuthor(int authorMasterId);
        void Save();
        int GetMasterAuthorIdByAuthor(int authorId);
        IQueryable<Author> GetAuthorsByMasterAuthor(int masterAuthorId);
        IQueryable<Author> SearchAuthorsByMask(string searchString);
        IQueryable<Book> SearchBooksByMask(string searchString);
        IQueryable<BookContent> SearchBooksByContent(string searchContent);
    }
}