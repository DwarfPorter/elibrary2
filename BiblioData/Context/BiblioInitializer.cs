﻿using BiblioData.DbModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioData.Context
{
    public class BiblioInitializer : BiblioGenreInitializer
    {
        protected override void Seed(BiblioContext context)
        {
            base.Seed(context);

            var tolstoi = new Author { AuthorId = 1, FirstName = "Л.Н.Толстой" };
            var chehov = new Author { AuthorId = 2, FirstName = "А.П.Чехов" };
            var pushkin = new Author { AuthorId = 3, FirstName = "А.С.Пушкин" };
            var zoshenko = new Author { AuthorId = 4, FirstName = "Зощенко" };
            var bstrugatski = new Author { AuthorId = 5, FirstName = "Б.Стругацкий" };
            var astrugatski = new Author { AuthorId = 6, FirstName = "А.Стругацкий" };
            var ilf = new Author { AuthorId = 7, FirstName = "Ильф" };
            var petrov = new Author { AuthorId = 8, FirstName = "Петров" };
            var pushkin2 = new Author { AuthorId = 9, FirstName = "Пушкин", MasterAuthor = pushkin };

            context.Authors.AddRange(new[] { tolstoi, chehov, pushkin, zoshenko });

/*            var novel = new Genre { GenreId = 1, Code = "Роман" };
            var comedy = new Genre { GenreId = 2, Code = "Комедия" };
            var feleton = new Genre { GenreId = 3, Code = "Фельетон" };
            var povest = new Genre { GenreId = 4, Code = "Повесть" };
            var poem = new Genre { GenreId = 5, Code = "Поэма" };*/

            var seqOstap = new Sequence { SequenceId = 1, Name = "Остап" };
            context.Sequences.Add(seqOstap);

           // context.Genres.AddRange(new[] { novel, comedy, feleton, povest, poem });

            context.Books.AddRange(new[]{
                new Book { BookId = 1, Title = "Война и Мир", GenreId = 1, Authors = new List<Author> { tolstoi } },
                new Book {BookId = 2, Title = "Руслан и Людмила", GenreId = 5, Authors = new List<Author>{pushkin} },
                new Book {BookId = 3, Title = "Евгений Онегин", GenreId = 1, Authors = new List<Author>{pushkin} },
                new Book { BookId = 4, Title = "Капитанская дочка", GenreId = 1, Authors = new List<Author> { pushkin2 } },
                new Book { BookId = 5, Title = "Вишневый сад", GenreId = 1, Authors = new List<Author> { chehov } },
                new Book { BookId = 6, Title = "Толстый и тонкий", GenreId = 3, Authors = new List<Author> { chehov } },
                new Book { BookId = 7, Title = "Не может быть", GenreId = 2, Authors = new List<Author> { zoshenko } },
                new Book { BookId = 8, Title = "Пикник на обочине", GenreId = 4, Authors = new List<Author> { bstrugatski, astrugatski } },
                new Book { BookId = 9, Title = "Трудно быть богом", GenreId = 4, Authors = new List<Author> { bstrugatski, astrugatski } },
                new Book { BookId = 10, Title = "понедельник начинается в субботу", GenreId = 4, Authors = new List<Author> { bstrugatski, astrugatski } },
                new Book { BookId = 11, Title = "Двенадцать стульев", GenreId = 1, SequenceId = 1, SequenceNumber = 1, Authors = new List<Author> { ilf, petrov } },
                new Book { BookId = 12, Title = "Золотой теленок", GenreId = 1, SequenceId = 1, SequenceNumber = 2, Authors = new List<Author> { ilf, petrov } }
            });


        }
    }
}
