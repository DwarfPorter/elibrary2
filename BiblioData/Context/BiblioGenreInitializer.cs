﻿using BiblioData.DbModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace BiblioData.Context
{
    public class BiblioGenreInitializer : CreateDatabaseIfNotExists<BiblioContext>
    {
        protected override void Seed(BiblioContext context)
        {

            context.GroupGenres.AddRange(new[] {
                new GroupGenre {GroupGenreId = 1, Description = "Фантастика (Научная фантастика и Фэнтези)" },
                new GroupGenre {GroupGenreId = 2, Description = "Детективы и Триллеры" },
                new GroupGenre {GroupGenreId = 3, Description = "Проза" },
                new GroupGenre {GroupGenreId = 4, Description = "Любовные романы" },
                new GroupGenre {GroupGenreId = 5, Description = "Приключения" },
                new GroupGenre {GroupGenreId = 6, Description = "Детское" },
                new GroupGenre {GroupGenreId = 7, Description = "Поэзия, Драматургия" },
                new GroupGenre {GroupGenreId = 8, Description = "Старинное" },
                new GroupGenre {GroupGenreId = 9, Description = "Наука, Образование" },
                new GroupGenre {GroupGenreId = 10, Description = "Компьютеры и Интернет" },
                new GroupGenre {GroupGenreId = 11, Description = "Справочная литература" },
                new GroupGenre {GroupGenreId = 12, Description = "Документальная литература" },
                new GroupGenre {GroupGenreId = 13, Description = "Религия и духовность" },
                new GroupGenre {GroupGenreId = 14, Description = "Юмор" },
                new GroupGenre {GroupGenreId = 15, Description = "Домоводство (Дом и семья)" }
            });

            context.Genres.AddRange(new[] {
                new Genre {GenreId = 1, Code = "sf_history", Description = "Альтернативная история", GroupGenreId = 1 },
                new Genre {GenreId = 2, Code = "sf_action", Description = "Боевая фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 3, Code = "sf_epic", Description = "Эпическая  фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 4, Code = "sf_heroic", Description = "Героическая фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 5, Code = "sf_detective", Description = "Детективная фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 6, Code = "sf_cyberpunk", Description = "Киберпанк", GroupGenreId = 1 },
                new Genre {GenreId = 7, Code = "sf_space", Description = "Космическая фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 8, Code = "sf_social", Description = "Социально-психологическая фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 9, Code = "sf_horror", Description = "Ужасы и Мистика", GroupGenreId = 1 },
                new Genre {GenreId = 10, Code = "sf_humor", Description = "Юмористическая фантастика", GroupGenreId = 1 },
                new Genre {GenreId = 11, Code = "sf_fantasy", Description = "Фэнтези", GroupGenreId = 1 },
                new Genre {GenreId = 12, Code = "sf", Description = "Научная Фантастика", GroupGenreId = 1 },

                new Genre {GenreId = 13, Code = "det_classic", Description = "Классический детектив", GroupGenreId = 2 },
                new Genre {GenreId = 14, Code = "det_police", Description = "Полицейский детектив", GroupGenreId = 2 },
                new Genre {GenreId = 15, Code = "det_action", Description = "Боевик", GroupGenreId = 2 },
                new Genre {GenreId = 16, Code = "det_irony", Description = "Иронический детектив", GroupGenreId = 2 },
                new Genre {GenreId = 17, Code = "det_history", Description = "Исторический детектив", GroupGenreId = 2 },
                new Genre {GenreId = 18, Code = "det_espionage", Description = "Шпионский детектив", GroupGenreId = 2 },
                new Genre {GenreId = 19, Code = "det_crime", Description = "Криминальный детектив", GroupGenreId = 2 },
                new Genre {GenreId = 20, Code = "det_political", Description = "Политический детектив", GroupGenreId = 2 },
                new Genre {GenreId = 21, Code = "det_maniac", Description = "Маньяки", GroupGenreId = 2 },
                new Genre {GenreId = 22, Code = "det_hard", Description = "Крутой детектив", GroupGenreId = 2 },
                new Genre {GenreId = 23, Code = "thriller", Description = "Триллер", GroupGenreId = 2 },
                new Genre {GenreId = 24, Code = "detective", Description = "Детектив (не относящийся в прочие категории)", GroupGenreId = 2 },

                new Genre {GenreId = 25, Code = "prose_classic", Description = "Классическая проза", GroupGenreId = 3 },
                new Genre {GenreId = 26, Code = "prose_history", Description = "Историческая проза", GroupGenreId = 3 },
                new Genre {GenreId = 27, Code = "prose_contemporary", Description = "Современная проза", GroupGenreId = 3 },
                new Genre {GenreId = 28, Code = "prose_counter", Description = "Контркультура", GroupGenreId = 3 },
                new Genre {GenreId = 29, Code = "prose_rus_classic", Description = "Русская классическая проза", GroupGenreId = 3 },
                new Genre {GenreId = 30, Code = "prose_su_classics", Description = "Советская классическая проза", GroupGenreId = 3 },

                new Genre {GenreId = 31, Code = "love_contemporary", Description = "Современные любовные романы", GroupGenreId = 4 },
                new Genre {GenreId = 32, Code = "love_history", Description = "Исторические любовные романы", GroupGenreId = 4 },
                new Genre {GenreId = 33, Code = "love_detective", Description = "Остросюжетные любовные романы", GroupGenreId = 4 },
                new Genre {GenreId = 34, Code = "love_short", Description = "Короткие любовные романы", GroupGenreId = 4 },
                new Genre {GenreId = 35, Code = "love_erotica", Description = "Эротика", GroupGenreId = 4 },

                new Genre {GenreId = 36, Code = "adv_western", Description = "Вестерн", GroupGenreId = 5 },
                new Genre {GenreId = 37, Code = "adv_history", Description = "Исторические приключения", GroupGenreId = 5 },
                new Genre {GenreId = 38, Code = "adv_indian", Description = "Приключения про индейцев", GroupGenreId = 5 },
                new Genre {GenreId = 39, Code = "adv_maritime", Description = "Морские приключения", GroupGenreId = 5 },
                new Genre {GenreId = 41, Code = "adv_geo", Description = "Путешествия и география", GroupGenreId = 5 },
                new Genre {GenreId = 42, Code = "adv_animal", Description = "Природа и животные", GroupGenreId = 5 },
                new Genre {GenreId = 43, Code = "adventure", Description = "Прочие приключения (то, что не вошло в другие категории)", GroupGenreId = 5 },

                new Genre {GenreId = 44, Code = "child_tale", Description = "Сказка", GroupGenreId = 6 },
                new Genre {GenreId = 45, Code = "child_verse", Description = "Детские стихи", GroupGenreId = 6 },
                new Genre {GenreId = 46, Code = "child_prose", Description = "Детскиая проза", GroupGenreId = 6 },
                new Genre {GenreId = 47, Code = "child_sf", Description = "Детская фантастика", GroupGenreId = 6 },
                new Genre {GenreId = 48, Code = "child_det", Description = "Детские остросюжетные", GroupGenreId = 6 },
                new Genre {GenreId = 49, Code = "child_adv", Description = "Детские приключения", GroupGenreId = 6 },
                new Genre {GenreId = 50, Code = "child_education", Description = "Детская образовательная литература", GroupGenreId = 6 },
                new Genre {GenreId = 51, Code = "children", Description = "Прочая детская литература (то, что не вошло в другие категории)", GroupGenreId = 6 },

                new Genre {GenreId = 52, Code = "poetry", Description = "Поэзия", GroupGenreId = 7 },
                new Genre {GenreId = 53, Code = "dramaturgy", Description = "Драматургия", GroupGenreId = 7 },

                new Genre {GenreId = 54, Code = "antique_ant", Description = "Античная литература", GroupGenreId = 8 },
                new Genre {GenreId = 55, Code = "antique_european", Description = "Европейская старинная литература", GroupGenreId = 8 },
                new Genre {GenreId = 56, Code = "antique_russian", Description = "Древнерусская литература", GroupGenreId = 8 },
                new Genre {GenreId = 57, Code = "antique_east", Description = "Древневосточная литература", GroupGenreId = 8 },
                new Genre {GenreId = 58, Code = "antique_myths", Description = "Мифы. Легенды. Эпос", GroupGenreId = 8 },
                new Genre {GenreId = 59, Code = "antique", Description = "Прочая старинная литература (то, что не вошло в другие категории)", GroupGenreId = 8 },

                new Genre {GenreId = 60, Code = "sci_history", Description = "История", GroupGenreId = 9 },
                new Genre {GenreId = 61, Code = "sci_psychology", Description = "Психология", GroupGenreId = 9 },
                new Genre {GenreId = 62, Code = "sci_culture", Description = "Культурология", GroupGenreId = 9 },
                new Genre {GenreId = 63, Code = "sci_religion", Description = "Религиоведение", GroupGenreId = 9 },
                new Genre {GenreId = 64, Code = "sci_philosophy", Description = "Философия", GroupGenreId = 9 },
                new Genre {GenreId = 65, Code = "ci_politics", Description = "Политика", GroupGenreId = 9 },
                new Genre {GenreId = 66, Code = "sci_business", Description = "Деловая литература", GroupGenreId = 9 },
                new Genre {GenreId = 67, Code = "sci_juris", Description = "Юриспруденция", GroupGenreId = 9 },
                new Genre {GenreId = 68, Code = "sci_linguistic", Description = "Языкознание", GroupGenreId = 9 },
                new Genre {GenreId = 69, Code = "sci_medicine", Description = "Медицина", GroupGenreId = 9 },
                new Genre {GenreId = 70, Code = "sci_phys", Description = "Физика", GroupGenreId = 9 },
                new Genre {GenreId = 71, Code = "sci_math", Description = "Математика", GroupGenreId = 9 },
                new Genre {GenreId = 72, Code = "sci_chem", Description = "Химия", GroupGenreId = 9 },
                new Genre {GenreId = 73, Code = "sci_biology", Description = "Биология", GroupGenreId = 9 },
                new Genre {GenreId = 74, Code = "sci_tech", Description = "Технические науки", GroupGenreId = 9 },
                new Genre {GenreId = 75, Code = "science", Description = "Прочая научная литература (то, что не вошло в другие категории)", GroupGenreId = 9 },

                new Genre {GenreId = 76, Code = "comp_www", Description = "Интернет", GroupGenreId = 10 },
                new Genre {GenreId = 77, Code = "comp_programming", Description = "Программирование", GroupGenreId = 10 },
                new Genre {GenreId = 78, Code = "comp_hard", Description = "Аппаратное обеспечение", GroupGenreId = 10 },
                new Genre {GenreId = 79, Code = "comp_soft", Description = "Программы", GroupGenreId = 10 },
                new Genre {GenreId = 80, Code = "comp_db", Description = "Базы данных", GroupGenreId = 10 },
                new Genre {GenreId = 81, Code = "comp_osnet", Description = "ОС и Сети", GroupGenreId = 10 },
                new Genre {GenreId = 82, Code = "computers", Description = "Прочая околокомпьтерная литература (то, что не вошло в другие категории)", GroupGenreId = 10 },

                new Genre {GenreId = 83, Code = "ref_encyc", Description = "Энциклопедии", GroupGenreId = 11 },
                new Genre {GenreId = 84, Code = "ref_dict", Description = "Словари", GroupGenreId = 11 },
                new Genre {GenreId = 85, Code = "ref_ref", Description = "Справочники", GroupGenreId = 11 },
                new Genre {GenreId = 86, Code = "ref_guide", Description = "Руководства", GroupGenreId = 11 },
                new Genre {GenreId = 87, Code = "reference", Description = "Прочая справочная литература (то, что не вошло в другие категории)", GroupGenreId = 11 },

                new Genre {GenreId = 88, Code = "nonf_biography", Description = "Биографии и Мемуары", GroupGenreId = 12 },
                new Genre {GenreId = 89, Code = "nonf_publicism", Description = "Публицистика", GroupGenreId = 12 },
                new Genre {GenreId = 90, Code = "nonf_criticism", Description = "Критика", GroupGenreId = 12 },
                new Genre {GenreId = 91, Code = "design", Description = "Искусство и Дизайн", GroupGenreId = 12 },
                new Genre {GenreId = 92, Code = "nonfiction", Description = "Прочая документальная литература (то, что не вошло в другие категории)", GroupGenreId = 12 },

                new Genre {GenreId = 93, Code = "religion_rel", Description = "Религия", GroupGenreId = 13 },
                new Genre {GenreId = 94, Code = "religion_esoterics", Description = "Эзотерика", GroupGenreId = 13 },
                new Genre {GenreId = 95, Code = "religion_self", Description = "Самосовершенствование", GroupGenreId = 13 },
                new Genre {GenreId = 96, Code = "religion", Description = "Прочая религионая литература (то, что не вошло в другие категории)", GroupGenreId = 13 },

                new Genre {GenreId = 97, Code = "humor_anecdote", Description = "Анекдоты", GroupGenreId = 14 },
                new Genre {GenreId = 98, Code = "humor_prose", Description = "Юмористическая проза", GroupGenreId = 14 },
                new Genre {GenreId = 99, Code = "humor_verse", Description = "Юмористические стихи", GroupGenreId = 14 },
                new Genre {GenreId = 100, Code = "humor", Description = "Прочий юмор (то, что не вошло в другие категории)", GroupGenreId = 14 },

                new Genre {GenreId = 101, Code = "home_cooking", Description = "Кулинария", GroupGenreId = 15 },
                new Genre {GenreId = 102, Code = "home_pets", Description = "Домашние животные", GroupGenreId = 15 },
                new Genre {GenreId = 103, Code = "home_crafts", Description = "Хобби и ремесла", GroupGenreId = 15 },
                new Genre {GenreId = 104, Code = "home_entertain", Description = "Развлечения", GroupGenreId = 15 },
                new Genre {GenreId = 105, Code = "home_health", Description = "Здоровье", GroupGenreId = 15 },
                new Genre {GenreId = 106, Code = "home_garden", Description = "Сад и огород", GroupGenreId = 15 },
                new Genre {GenreId = 107, Code = "home_diy", Description = "Сделай сам", GroupGenreId = 15 },
                new Genre {GenreId = 108, Code = "home_sport", Description = "Спорт", GroupGenreId = 15 },
                new Genre {GenreId = 109, Code = "home_sex", Description = "Эротика, Секс", GroupGenreId = 15 },
                new Genre {GenreId = 110, Code = "home", Description = "Прочиее домоводство (то, что не вошло в другие категории)", GroupGenreId = 15 },

             });
            base.Seed(context);
        }
    }
}
