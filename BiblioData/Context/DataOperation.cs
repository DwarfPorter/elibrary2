﻿using BiblioData.DbModels;
using System.Linq;
using System;
using System.Collections.Generic;

namespace BiblioData.Context
{
    public class DataOperation : IDataOperation, IDisposable
    {
        private BiblioContext _db;

        public DataOperation()
        {
            _db = new BiblioContext();
        }

        public bool ExistsDb()
        {
            return _db.Database.Exists();
        }

        public Genre SearchGenre(string name)
        {
            Genre answer;
            answer = _db.Genres.FirstOrDefault(g => name == g.Code);
            return answer;
        }

        public Sequence SearchSequence(string name)
        {
            Sequence answer;
            answer = _db.Sequences.FirstOrDefault(s => name == s.Name);
            return answer;
        }

        public Book SearchBookByFileName(string fileName)
        {
            if (fileName == null) return null;
            return _db.Books.FirstOrDefault(b => (fileName == b.FileName));
        }

        public Author SearchAuthor(string firstName, string lastName = null)
        {
            if (firstName == null && lastName == null) return null;
            return _db.Authors.FirstOrDefault(a => (firstName == a.FirstName)
                                                && (lastName == a.LastName)
                                                );
        }

        public Book SearchBookByMD5(string md5)
        {
            if (md5 == null) return null;
            return _db.Books.FirstOrDefault(b => (md5 == b.MD5));
        }

        public void AddAuthor(Author author)
        {
            _db.Authors.Add(author);
        }

        public void AddBook(Book book)
        {
            _db.Books.Add(book);
        }

        public void AddGenre(Genre genre)
        {
            _db.Genres.Add(genre);
        }

        public void AddSequence(Sequence sequence)
        {
            _db.Sequences.Add(sequence);
        }

        public void AddContent(Content content)
        {
            _db.Contents.Add(content);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public IQueryable<Book> GetBooks()
        {
            return _db.Books;
        }
        public Book GetBook(int bookId)
        {
            return _db.Books.Include("Authors").Include("Contents").FirstOrDefault(b => b.BookId == bookId);
        }

        public IQueryable<Author> GetAuthors()
        {
            return _db.Authors;
        }

        public IQueryable<MasterAuthor> GetMasterAuthors()
        {
            var answer = from a in _db.Authors
                         join ma in _db.Authors on (a.MasterAuthor != null ? a.MasterAuthor.AuthorId : a.AuthorId) equals ma.AuthorId
                         select new MasterAuthor
                         {
                             AuthorId = a.AuthorId,
                             MasterAuthorId = ma.AuthorId,
                             FirstName = ma.FirstName,
                             MiddleName = ma.MiddleName,
                             LastName = ma.LastName,
                             NickName = ma.NickName
                             //Books = a.Books
                         };
            return answer;
        }

        public IQueryable<MasterGroupAuthor> GetMasterGroupAuthors()
        {
            var answer = GetMasterAuthors().Select(a => new MasterGroupAuthor { MasterAuthorId = a.MasterAuthorId, FirstName = a.FirstName, MiddleName = a.MiddleName, LastName = a.LastName, NickName = a.NickName }).Distinct();
            return answer;
        }

        public MasterGroupAuthor GetMasterGroupAuthor(int masterAuthorId)
        {
            return GetMasterGroupAuthors().FirstOrDefault(mga => mga.MasterAuthorId == masterAuthorId);
        }

        public IQueryable<MasterAuthor> GetMasterAuthor(int authorId)
        {
            var answer = GetMasterAuthors().Where(ma => ma.MasterAuthorId == authorId);
            return answer;
        }

        public IQueryable<Book> GetBooksByMasterAuthor(int authorMasterId)
        {
            var answer = new List<Book>();
            foreach (var author in GetMasterAuthor(authorMasterId).ToList())
            {
                answer.AddRange(_db.Books.Include("Authors").Where(b => b.Authors.Any(a => a.AuthorId == author.AuthorId)));
            }
            return answer.AsQueryable();
        }

        public int GetMasterAuthorIdByAuthor(int authorId)
        {
            var masterAuthor = GetMasterAuthors().FirstOrDefault(mga => mga.AuthorId == authorId);
            if (masterAuthor == null) return 0;
            return masterAuthor.MasterAuthorId;
        }

        public IQueryable<Author> GetAuthorsByMasterAuthor(int masterAuthorId)
        {
            IQueryable<Author> answer = from a in _db.Authors where a.MasterAuthor != null && a.MasterAuthor.AuthorId == masterAuthorId select a ;
            return answer;
        }

        public IQueryable<Author> SearchAuthorsByMask(string searchString)
        {
            return _db.Authors.Where(a => a.FirstName.Contains(searchString) || a.LastName.Contains(searchString) || a.MiddleName.Contains(searchString) || a.NickName.Contains(searchString));
        }

        public IQueryable<Book> SearchBooksByMask(string searchString)
        {
            return _db.Books.Where(b => b.Title.Contains(searchString) || b.Annotation.Contains(searchString));
        }

        public IQueryable<BookContent> SearchBooksByContent(string searchContent)
        {
            var answer = from b in _db.Books
                         join c in _db.Contents on b.BookId equals c.BookId
                         where c.Header.Contains(searchContent)
                         select new BookContent {BookId = b.BookId, Title = b.Title, Content = c.Header };
            return answer;
        }

        public void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }

    }
}
