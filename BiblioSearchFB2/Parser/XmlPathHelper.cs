﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BiblioSearchFB2.Parser
{
    public static class XmlPathHelper
    {
        public static XmlElement GetFirstElementByTagName(this XmlElement root, string tagName)
        {
            XmlElement refField = root.GetElementsByTagName(tagName).Item(0) as XmlElement;
            if (refField == null)
                refField = root.GetElementsByTagName(tagName, "*").Item(0) as XmlElement;
            return refField;
        }

        private static XmlElement GetElementByPath(this XmlElement doc, string path)
        {
            var pathSplit = path.Split('/');

            XmlElement currentElement = doc;
            foreach (string partPath in pathSplit)
            {
                currentElement = currentElement.GetFirstElementByTagName(partPath);
                if (currentElement == null)
                {
                    return null;
                }
            }
            return currentElement;
        }

        public static string GetValueByPath(this XmlElement doc, string path)
        {

            //XmlElement element = root.SelectSingleNode(path) as XmlElement; XPath needs namespace. because xml already has namespace then i need read all namespace before use XPath. simple read path without xPath

            XmlElement currentElement = GetElementByPath(doc, path);
            if (currentElement == null) return null;
            var text = currentElement.FirstChild as XmlText;
            if (text == null) return null;
            if (string.IsNullOrWhiteSpace(text.Value)) return null;
            return text.Value.Trim();

        }

        public static IList<string> GetValuesByTagName(this XmlDocument root, string tagName)
        {
            var elements = root.GetElementsByTagName(tagName);
            return GetAllTexts(elements);
        }
        
        private static IList<string> GetAllTexts(XmlNodeList elements)
        {
            var answer = new List<string>();
            for (int i = 0; i < elements.Count; i++)
            {
                XmlElement element = elements.Item(i) as XmlElement;
                string innerText = string.Empty;
                var text = element.FirstChild as XmlText;
                if (text == null)
                {
                    answer.AddRange(GetAllTexts(element.ChildNodes));
                }
                else
                {
                    answer.Add(text.Value);
                }
            }
            return answer;
        }

        public static string GetAttributeValueByPath(this XmlElement doc, string path, string attribute)
        {

            //XmlElement element = root.SelectSingleNode(path) as XmlElement; XPath needs namespace. because xml already has namespace then i need read all namespace before use XPath. simple read path without xPath


            XmlElement currentElement = GetElementByPath(doc, path);
            if (currentElement == null) return null;
            string attributeValue = currentElement.GetAttribute(attribute);
            if (attributeValue == null) return null;
            return attributeValue.Trim();
        }

    }
}
