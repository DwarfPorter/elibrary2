﻿using BiblioSearchFB2.SearchModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Drawing;

namespace BiblioSearchFB2.Parser
{
    public static class ParserFB2
    {
        private static List<Author> GetAuthors(this XmlElement titleInfo)
        {
            var answer = new List<Author>();

            var authors = titleInfo.GetElementsByTagName("author");
            foreach (XmlElement author in authors)
            {
                answer.Add(new Author()
                {
                    FirstName = author.GetValueByPath("first-name"),
                    MiddleName = author.GetValueByPath("middle-name"),
                    LastName = author.GetValueByPath("last-name"),
                    NickName = author.GetValueByPath("nickname")
                });
            }
            return answer;
        }

        public static Book GetBook(this XmlDocument book, string fileName)
        {
            var answer = new Book();
            var titleInfo = book.GetElementsByTagName("title-info").Item(0) as XmlElement;

            answer.FileName = fileName;
            answer.MachineName = System.Environment.MachineName;

            answer.Title = titleInfo.GetValueByPath("book-title");
            answer.Genre = titleInfo.GetValueByPath("genre");
            answer.Annotation = titleInfo.GetValueByPath("annotation/p");
            answer.KeyWords = titleInfo.GetValueByPath("keywords");

            answer.Sequence = titleInfo.GetAttributeValueByPath("sequence", "name");
            var numberSequence = titleInfo.GetAttributeValueByPath("sequence", "number"); 
            if (numberSequence != null)
            {
                int number;
                if (int.TryParse(numberSequence, out number)) answer.SequenceNumber = number;
            }

            /*    var sequence = titleInfo.GetElementsByTagName("sequence").Item(0) as XmlElement;
                if (sequence != null)
                {
                    answer.Sequence = sequence.GetAttribute("name");
                    var numberSequence = sequence.GetAttribute("number");
                    if (numberSequence != null)
                    {
                        int number;
                        if (int.TryParse(numberSequence, out number)) answer.SequenceNumber = number;
                    }
                }*/

            answer.Contents = book.GetValuesByTagName("title");

            answer.Authors = titleInfo.GetAuthors();
            if (!string.IsNullOrWhiteSpace(fileName)) answer.MD5 = GetMD5(fileName); // For test it is not need

            return answer;
        }

        public static string GetMD5(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }
        }

        public static Bitmap GetCover(string path)
        {
            return GetXmlFromFile(path).GetCover();
        }

        public static Bitmap GetCover(this XmlDocument book)
        {
            var titleInfo = book.GetElementsByTagName("title-info").Item(0) as XmlElement;
            string linkToImage = titleInfo.GetAttributeValueByPath("coverpage/image", "l:href");
            if (linkToImage == null) return null; 
            if (!linkToImage.StartsWith("#")) return null;
            linkToImage = linkToImage.Substring(1);
            var binaries = book.GetElementsByTagName("binary");
            string ImageText = null;
            foreach (XmlElement item in binaries)
            {
                string idCover = item.GetAttribute("id");
                if (idCover == linkToImage)
                {
                    var text = item.FirstChild as XmlText;
                    if (text == null) return null;
                    ImageText = text.Value;
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(ImageText)) return null;

            Byte[] bitmapData = Convert.FromBase64String(FixBase64ForImage(ImageText));
            using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData))
            {
                Bitmap bitImage = new Bitmap((Bitmap)Image.FromStream(streamBitmap));
                return bitImage;
            }
            
        }

        private static string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", String.Empty); sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }

        public static XmlDocument GetXmlFromFile(string path)
        {
            var bookFb2 = new XmlDocument();
            string xmlString = File.ReadAllText(path); 
            if (xmlString == null) return null;
            bookFb2.LoadXml(xmlString);
            return bookFb2;
        }


    }
}
