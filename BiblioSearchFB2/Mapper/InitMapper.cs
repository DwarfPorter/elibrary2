﻿using AutoMapper;

namespace BiblioSearchFB2.Mapper
{
    public static class InitMapper
    {
        public static IMapper Mapper()
        {

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SearchModels.Book, BiblioData.DbModels.Book>()
                    .ForMember(d => d.Genre, o => o.Ignore())
                    .ForMember(d => d.Sequence, o => o.Ignore())
                    .ForMember(d => d.Authors, o => o.Ignore())
                    .ForMember(d => d.Contents, o => o.Ignore())
                    .ForMember(d => d.IsActive, o => o.MapFrom(s => true));
                cfg.CreateMap<SearchModels.Author, BiblioData.DbModels.Author>();
                cfg.CreateMap<SearchModels.Book, BiblioData.DbModels.Genre>()
                    .ForMember(d => d.Code, o => o.MapFrom(s => s.Genre));
                cfg.CreateMap<SearchModels.Book, BiblioData.DbModels.Sequence>()
                    .ForMember(d => d.Name, o => o.MapFrom(s => s.Sequence));
            });
            return config.CreateMapper();
        }
    }
}
