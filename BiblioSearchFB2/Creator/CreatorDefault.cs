﻿using System;
using BiblioData.Context;
using BiblioLogger;
using BiblioSearchFB2.Searcher;

namespace BiblioSearchFB2.Creator
{
    public class CreatorDefault : ICreator
    {
        public IBiblioLog CreateBiblioLog()
        {
            return (new BiblioLogger.Creator.CreatorDefault()).CreateBiblioLog();
        }

        public IDataOperation CreateDataOperation()
        {
            return (new BiblioData.Creator.CreatorDefault()).CreateDataOperation();
        }

        public IDirOperation CreateDirOperation()
        {
            return new SystemDirOperation();
        }

        public ISearchController CreateSearchController()
        {
            return new SearchController();
        }
    }
}
