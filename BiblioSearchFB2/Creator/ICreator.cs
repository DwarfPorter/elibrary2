﻿using BiblioLogger;
using BiblioSearchFB2.Searcher;

namespace BiblioSearchFB2.Creator
{
    public interface ICreator : BiblioLogger.Creator.ICreator, BiblioData.Creator.ICreator
    {
        IDirOperation CreateDirOperation();
        ISearchController CreateSearchController();
    }
}
