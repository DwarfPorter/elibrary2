﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioSearchFB2.Searcher
{
    public interface IDirOperation
    {
        IEnumerable<string> GetDrives();
        IEnumerable<string> GetDirectories(string folder);
        IEnumerable<string> GetFiles(string folder);
    }
}
