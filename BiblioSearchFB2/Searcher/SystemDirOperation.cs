﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BiblioSearchFB2.Searcher
{
    public class SystemDirOperation : IDirOperation
    {
        public IEnumerable<string> GetDirectories(string folder)
        {
            return Directory.GetDirectories(folder);
        }

        public IEnumerable<string> GetDrives()
        {
            var drives = DriveInfo.GetDrives();
            return drives.Select(d => d.Name);
        }

        public IEnumerable<string> GetFiles(string folder)
        {
            return Directory.GetFiles(folder, "*.fb2");
        }
    }
}
