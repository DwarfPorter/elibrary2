﻿using AutoMapper;
using BiblioData.Context;
using BiblioLogger;
using BiblioSearchFB2.Creator;
using BiblioSearchFB2.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using BiblioSearchFB2.SearchModels;

namespace BiblioSearchFB2.Searcher
{
    public class SearchController : ISearchController
    {
        private static IMapper _mapper;

        private ICreator _creator;
        private IBiblioLog _biblioLog;

        static SearchController()
        {
            _mapper = Mapper.InitMapper.Mapper();
        }

        public SearchController() : this(new CreatorDefault())
        { }

        public SearchController(ICreator creator)
        {
            _creator = creator;
            _biblioLog = creator.CreateBiblioLog();
        }

        public void Execute(string path)
        {
            var searcher = new SearcherFB2(ProcessFile);
            if (string.IsNullOrWhiteSpace(path))
            {
                searcher.SeekAndApplyAllDrives();
            }
            else
            {
                searcher.SeekAndApplyAllFiles(path);
            }
        }

        internal List<BiblioData.DbModels.Author> GetDbAuthorsFromBook(IDataOperation dbo, SearchModels.Book book)
        {
            var authorsdb = new List<BiblioData.DbModels.Author>();
            foreach (var author in book.Authors)
            {
                var authordb = dbo.SearchAuthor(author.FirstName, author.LastName);
                if (authordb == null)
                {
                    //flNeedSave = true;
                    if (author.IsEmpty()) continue;
                    authordb = new BiblioData.DbModels.Author();
                    _mapper.Map(author, authordb);
                    dbo.AddAuthor(authordb);
                }
                else
                {
                    if (authordb.MiddleName == null) authordb.MiddleName = author.MiddleName;
                    if (authordb.NickName == null) authordb.NickName = author.NickName;
                }
                authorsdb.Add(authordb);
            }
            return authorsdb;
        }

        internal BiblioData.DbModels.Genre GetDbGenreFromBook(IDataOperation dbo, SearchModels.Book book)
        {
            var genreDb = dbo.SearchGenre(book.Genre);
            if (genreDb == null)
            {
                genreDb = new BiblioData.DbModels.Genre();
                _mapper.Map(book, genreDb);
                dbo.AddGenre(genreDb);
            }
            return genreDb;
        }

        internal BiblioData.DbModels.Sequence GetDbSequenceFromBook(IDataOperation dbo, Book book)
        {
            if (string.IsNullOrWhiteSpace(book.Sequence)) return null;
            var sequenceDb = dbo.SearchSequence(book.Sequence);
            if (sequenceDb == null)
            {
                sequenceDb = new BiblioData.DbModels.Sequence();
                _mapper.Map(book, sequenceDb);
                dbo.AddSequence(sequenceDb);
            }
            return sequenceDb;
        }

        internal List<BiblioData.DbModels.Content> GetDbContentFromBook(IDataOperation dbo, Book book)
        {
            List<BiblioData.DbModels.Content> answer = new List<BiblioData.DbModels.Content>();
            foreach (var textContent in book.Contents)
            {
                var content = new BiblioData.DbModels.Content();
                content.Header = textContent;
                dbo.AddContent(content);
                answer.Add(content); 
            }
            return answer;
        }

        public void ProcessFile(string path)
        {
            var bookFb2 = new XmlDocument();
            try
            {
                ReadXmlFromFile(bookFb2, path);
                /*string xmlString = ReadTextFromFile(path);
                if (xmlString == null) return;
                bookFb2.LoadXml(xmlString);*/
            }
            catch (Exception e)
            {
                _biblioLog.Fatal("Exception occurs when load fb2", e);
                throw;
            }
            ProcessFile(bookFb2, path);

        }

        private string ReadTextFromFile(string path)
        {
            string xmlString = null;
            bool complite = false;
            int cntTry = 0;
            while (!complite)
            {
                Thread.Sleep(200);
                cntTry++;
                if (cntTry > 1000) return null;
                try
                {
                    xmlString = File.ReadAllText(path);
                    complite = true;
                }
                catch (IOException e)
                {
                    _biblioLog.Fatal("Try Access", e);
                }
            }
            return xmlString;
        }

        private bool ReadXmlFromFile(XmlDocument bookFb2, string path)
        {
            int cntTry = 0;
            bool complete = false;
            while (!complete)
            {
                Thread.Sleep(200);
                cntTry++;
                if (cntTry > 1000) return false;
                try
                {
                    bookFb2.Load(path);
                    complete = true;
                }
                catch(IOException e)
                {
                    _biblioLog.Fatal("Try Access", e);
                }
            }
            return complete;

        }

        public void ProcessFileIfRemoved(string path)
        {
            using (var dbo = _creator.CreateDataOperation())
            {
                var book = dbo.SearchBookByFileName(path);
                book.IsActive = false;
                dbo.Save();
            }

        }

        internal void ProcessFile(XmlDocument bookFb2, string path)
        {
            try
            {
                var book = bookFb2.GetBook(path);

                using (var dbo = _creator.CreateDataOperation())
                {
                    if (book.Title == null) return;
                    var book1 = dbo.SearchBookByFileName(path);
                    if (book1 != null)
                    {
                        book1.IsActive = true;
                        dbo.Save();
                        return;
                    }
                    var bookmd5 = dbo.SearchBookByMD5(book.MD5);
                    if (bookmd5 != null)
                    {
                        if (File.Exists(bookmd5.FileName))
                        {
                            _biblioLog.Info(string.Format("Book with path: {0} already exists by the path {1}", path, bookmd5.FileName));
                            File.Delete(path);
                            return;
                        }
                        else
                        {
                            bookmd5.IsActive = false;
                        }
                    }
                    var genreDb = GetDbGenreFromBook(dbo, book);
                    var sequenceDb = GetDbSequenceFromBook(dbo, book);
                    var authorsdb = GetDbAuthorsFromBook(dbo, book);
                    var bookdb = new BiblioData.DbModels.Book();
                    _mapper.Map(book, bookdb);

                    dbo.AddBook(bookdb);

                    var contents = GetDbContentFromBook(dbo, book);
                    contents.ForEach(c =>
                    {
                        bookdb.Contents.Add(c);
                    });
                    
                    authorsdb.ForEach(a =>
                    {
                        bookdb.Authors.Add(a);
                        a.Books.Add(bookdb);
                    });

                    genreDb.Books.Add(bookdb);
                    if (sequenceDb != null) sequenceDb.Books.Add(bookdb);

                    dbo.Save();

                }

                _biblioLog.Info(string.Format("{0} : {3}, {1} {2} - {4}", book.FileName, book.Authors[0].FirstName, book.Authors[0].MiddleName ?? string.Empty, book.Authors[0].LastName, book.Title));
            }
            catch (Exception e)
            {
                _biblioLog.Fatal("Exception occurs when save", e);
                throw;
            }
        }

        public void ProcessFileIfRenamed(string oldPath, string newPath)
        {
            BiblioData.DbModels.Book book;
            using (var dbo = _creator.CreateDataOperation())
            {
                book = dbo.SearchBookByFileName(oldPath);
                if (book != null)
                {
                    book.FileName = newPath;
                    dbo.Save();
                }
            }

            if (book == null)
            {
                ProcessFile(newPath);
            }
        }

    }
}
