﻿using BiblioLogger;
using BiblioSearchFB2.Creator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioSearchFB2.Searcher
{
    public class SearcherFB2
    {
        private IDirOperation _dirOperation;
        private Action<string> _fileAction;
        private IBiblioLog _biblioLogger;

        public SearcherFB2(Action<string> fileAction) : this(fileAction, new CreatorDefault())
        {

        }

        public SearcherFB2(Action<string> fileAction, ICreator creator)
        {
            this._fileAction = fileAction;
            _dirOperation = creator.CreateDirOperation();
            _biblioLogger = creator.CreateBiblioLog();
        }

        public void SeekAndApplyAllDrives()
        {
            var drives = _dirOperation.GetDrives();

            foreach (var d in drives)
            {
                try
                {
                    SeekAndApplyAllFiles(d);
                }
                catch (Exception e)
                {
                    _biblioLogger.Fatal("Error when get drives", e);
                    //Console.WriteLine(e.Message);
                }
            }
        }

        public void SeekAndApplyAllFiles(string folder)
        {
            foreach (string file in _dirOperation.GetFiles(folder))
            {
                _fileAction(file);
            }
            foreach (string subDir in _dirOperation.GetDirectories(folder))
            {
                try
                {
                    SeekAndApplyAllFiles(subDir);
                }
                catch (Exception e)
                {
                    _biblioLogger.Fatal("Error when get files", e);
                    //Console.WriteLine(e.Message);
                }
            }
        }
    }
}
