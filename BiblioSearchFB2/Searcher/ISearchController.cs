﻿namespace BiblioSearchFB2.Searcher
{
    public interface ISearchController
    {
        void Execute(string path);
        void ProcessFile(string path);
        void ProcessFileIfRemoved(string path);
        void ProcessFileIfRenamed(string oldFielName, string newFileName);
    }
}