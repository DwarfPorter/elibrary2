﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioSearchFB2.SearchModels
{
    public class Author
    {
        //public long AuthorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

        public bool IsEmpty()
        {
            return (FirstName == null) && (LastName == null);
        }
    }
}
