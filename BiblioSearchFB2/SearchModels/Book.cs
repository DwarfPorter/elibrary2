﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioSearchFB2.SearchModels
{
    public class Book
    {
        //public long BookId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string Genre { get; set; }
        public string MD5 { get; set; }
        public string Sequence { get; set; }
        public int? SequenceNumber { get; set; }
        public string MachineName { get; set; }
        public string Annotation { get; set; }
        public string KeyWords { get; set; }

        public IList<Author> Authors { get; set; }
        public IList<string> Contents { get; set; }

    }
}
