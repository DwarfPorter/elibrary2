﻿using BiblioLogger.Creator;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioLogger
{
    public class BiblioLog : IBiblioLog
    {
        private const string _loggerName = "Biblio";
        private static readonly ILog _logger = LogManager.GetLogger(_loggerName);
        //private static ICreator creator = new CreatorDefault();

        static BiblioLog()
        {
            XmlConfigurator.Configure();
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Debug(string message)
        {
            if (!_logger.IsDebugEnabled) return;
            var stackTrace = new StackTrace();
            var frame = stackTrace.GetFrame(4);
            var method = frame.GetMethod();
            var typeName = method.DeclaringType.FullName;
            var methodName = method.Name;
            _logger.Debug(string.Format("[{0}.{1}] - {2}", typeName, methodName, message));
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(string message, Exception e)
        {
            _logger.Fatal(message, e);
        }
    }
}
