﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioLogger.Creator
{
    public class CreatorDefault : ICreator
    {
        public IBiblioLog CreateBiblioLog()
        {
            return new BiblioLog();
        }
    }
}
