﻿using System;

namespace BiblioLogger
{
    public interface IBiblioLog
    {
        void Debug(string message);
        void Fatal(string message);
        void Fatal(string message, Exception e);
        void Info(string message);
    }
}