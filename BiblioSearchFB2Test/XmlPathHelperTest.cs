﻿using NUnit.Framework;
using System.Xml;
using BiblioSearchFB2.Parser;
using System.Collections.Generic;
using System.Linq;

namespace BiblioSearchFB2.Parser.Tests
{
    [TestFixture]
    public class XmlPathHelperTests
    {

        [Test]
        public void GetFirstElementByTagNameTest()
        {
            string xmlString = "<root><Element>first</Element><Element>second</Element></root>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var root = xmlDoc.GetElementsByTagName("root").Item(0) as XmlElement; 
            var element1 = root.GetFirstElementByTagName("Element");
            Assert.AreEqual("first", (element1.FirstChild as XmlText).Value);
        }

        [Test]
        public void GetValueByPathTest()
        {
            string xmlString = "<root><Element><Code>first</Code></Element><Element>second</Element></root>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var root = xmlDoc.GetElementsByTagName("root").Item(0) as XmlElement; 
            var element1 = root.GetValueByPath("Element/Code");
            Assert.AreEqual("first", element1);
        }

        [Test]
        public void GetAttributeValueByPathTest()
        {
            string xmlString = "<root><Element><Code attr=\"fAttr\">first</Code></Element><Element>second</Element></root>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var root = xmlDoc.GetElementsByTagName("root").Item(0) as XmlElement;
            var attr1 = root.GetAttributeValueByPath("Element/Code", "attr");
            Assert.AreEqual("fAttr", attr1);
        }

        [Test]
        public void GetValuesByTagNameTest()
        {
            string xmlString = "<root><Title><p>text1</p><p>text2</p></Title><Section><Title><p>text3</p></Title></Section><Section><Title><p>text4</p></Title></Section><Title>text5</Title></root>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
           // var root = xmlDoc.GetElementsByTagName("root").Item(0) as XmlElement;
            List<string> titles = xmlDoc.GetValuesByTagName("Title").ToList();
            Assert.AreEqual(5, titles.Count());
            Assert.AreEqual("text1", titles[0]);
            Assert.AreEqual("text2", titles[1]);
            Assert.AreEqual("text3", titles[2]);
            Assert.AreEqual("text4", titles[3]);
            Assert.AreEqual("text5", titles[4]);
        }
    }
}