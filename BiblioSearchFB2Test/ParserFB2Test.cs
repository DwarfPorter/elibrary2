﻿using BiblioSearchFB2.Parser;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BiblioSearchFB2Test
{
    [TestFixture]
    public class ParserFB2Test
    {
        [Test]
        public void GetBookTest()
        {
            string xmlString = "<FictionBook xmlns=\"http://www.gribuser.ru/xml/fictionbook/2.0\" xmlns:l=\"http://www.w3.org/1999/xlink\"><description><title-info>" + 
            "<genre>sf_humor</genre>" + 
            "<author><first-name>Terry</first-name><last-name>Pratchet</last-name></author><book-title>Wonderful Morris</book-title><date/>" +
            "<annotation><p>About Cat</p></annotation><keywords>cat, rats</keywords>" +
            "<sequence name=\"Discworld\" number = \"28\"/></title-info></description></FictionBook>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            var book = xmlDoc.GetBook("");
            Assert.AreEqual("Wonderful Morris", book.Title);
            Assert.AreEqual("sf_humor", book.Genre);
            Assert.AreEqual("Discworld", book.Sequence);
            Assert.AreEqual(28, book.SequenceNumber);
            Assert.AreEqual("Terry", book.Authors[0].FirstName);
            Assert.AreEqual("Pratchet", book.Authors[0].LastName);
            Assert.AreEqual("About Cat", book.Annotation);
            Assert.AreEqual("cat, rats", book.KeyWords);

        }
    }
}
