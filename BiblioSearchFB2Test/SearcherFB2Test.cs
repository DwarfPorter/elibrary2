﻿using BiblioLogger;
using BiblioSearchFB2.Creator;
using BiblioSearchFB2.Searcher;
using Moq;
using NUnit.Framework;
using System;

namespace BiblioSearchFB2Test
{
    [TestFixture]
    public class SearcherFB2Test
    {
        public interface IFileAction
        {
            void FileAction(string file);
        }

        [Test]
        public void SeekAndApplyAllFilesTest()
        {

            var dirOperation = new Mock<IDirOperation>();
            dirOperation.Setup(d => d.GetDrives()).Returns(new string[] {@"C:\", @"D:\" });
            string[][] resultGetDirectories = new string[][] { new string[] { @"C:\Vova\", @"C:\Vlad\" }, new string[] { } };
            var i = 0;
            dirOperation.Setup(d => d.GetDirectories(It.IsAny<string>())).Returns(() => resultGetDirectories[i]).Callback(() => { if (i < 1) i++; });
            dirOperation.Setup(d => d.GetFiles(It.IsAny<string>())).Returns(new string[] { @"C:\Vova\first.fb2", @"C:\Vlad\second.fb2" });

            var biblioLog = new Mock<IBiblioLog>();

            ICreator creator = Mock.Of<ICreator>(c => c.CreateDirOperation() == dirOperation.Object
                                                && c.CreateBiblioLog() == biblioLog.Object);

            var mockFileAction = new Mock<IFileAction>();

            var searcherFB2 = new SearcherFB2(mockFileAction.Object.FileAction, creator);
            searcherFB2.SeekAndApplyAllFiles("");

            mockFileAction.Verify(fa => fa.FileAction(@"C:\Vova\first.fb2"), Times.Exactly(3)); //Each time by each dir
            mockFileAction.Verify(fa => fa.FileAction(@"C:\Vlad\second.fb2"), Times.Exactly(3));
            dirOperation.Verify(d => d.GetFiles(@"C:\Vova\"), Times.Once);   // once time by each directory include root
            dirOperation.Verify(d => d.GetFiles(@"C:\Vlad\"), Times.Once);   // once time by each directory include root
            dirOperation.Verify(d => d.GetDirectories(""), Times.Once);   // Root directory
            dirOperation.Verify(d => d.GetDirectories(@"C:\Vova\"), Times.Once);   // First directory
            dirOperation.Verify(d => d.GetDirectories(@"C:\Vlad\"), Times.Once);   // Second directory

        }

        [Test]
        public void SeekAndApplyAllDrivesTest()
        {
            var dirOperation = new Mock<IDirOperation>();
            dirOperation.Setup(d => d.GetDrives()).Returns(new string[] { @"C:\", @"D:\" });
            dirOperation.Setup(d => d.GetDirectories(It.IsAny<string>())).Returns(new string[] { });
            dirOperation.Setup(d => d.GetFiles(It.IsAny<string>())).Returns(new string[] { @"C:\Vova\first.fb2"});

            var biblioLog = new Mock<IBiblioLog>();

            ICreator creator = Mock.Of<ICreator>(c => c.CreateDirOperation() == dirOperation.Object
                                                && c.CreateBiblioLog() == biblioLog.Object);

            var cntActionCallBack = 0;
            var searcherFB2 = new SearcherFB2(str => cntActionCallBack++, creator);
            searcherFB2.SeekAndApplyAllDrives();

            Assert.AreEqual(2, cntActionCallBack); // Each time by each drive
            dirOperation.Verify(d => d.GetDrives(), Times.Once);
            dirOperation.Verify(d => d.GetDirectories(It.IsAny<string>()), Times.Exactly(2));
            dirOperation.Verify(d => d.GetFiles(@"C:\"), Times.Once);
            dirOperation.Verify(d => d.GetFiles(@"D:\"), Times.Once);



        }
    }
}
