﻿using BiblioData.Context;
using BiblioLogger;
using BiblioSearchFB2.Searcher;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BiblioSearchFB2Test
{
    [TestFixture]
    public class SearchControllerTest
    {
        [Test]
        public void GetDbGenreFromBookTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();

            var searcher = new SearchController();
            var genreActual = searcher.GetDbGenreFromBook(mockDbo.Object, new BiblioSearchFB2.SearchModels.Book { Genre = "TestGenre" });
            Assert.AreEqual("TestGenre", genreActual.Code);
            mockDbo.Verify(d => d.AddGenre(It.IsAny<BiblioData.DbModels.Genre>()), Times.Once);
            mockDbo.Verify(d => d.SearchGenre(It.IsAny<string>()), Times.Once);
            
        }

        [Test]
        public void GetDbSequenceFromBookTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();

            var searcher = new SearchController();
            var sequenceActual = searcher.GetDbSequenceFromBook(mockDbo.Object, new BiblioSearchFB2.SearchModels.Book { Sequence = "TestSequence" });
            Assert.AreEqual("TestSequence", sequenceActual.Name);
            mockDbo.Verify(d => d.AddSequence(It.IsAny<BiblioData.DbModels.Sequence>()), Times.Once);
            mockDbo.Verify(d => d.SearchSequence(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void GetDbAuthorsFromBookTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            var cntAuthor = 0;
            BiblioData.DbModels.Author[] authorsForTest = new BiblioData.DbModels.Author[2] { new BiblioData.DbModels.Author { FirstName = "Vova", LastName = "Vlad" }, null };
            mockDbo.Setup(d => d.SearchAuthor(It.IsAny<string>(), It.IsAny<string>())).Returns(() => authorsForTest[cntAuthor]).Callback(() => { if (cntAuthor == 0) cntAuthor++; });

            var searcher = new SearchController();
            var authorsActual = searcher.GetDbAuthorsFromBook(mockDbo.Object, new BiblioSearchFB2.SearchModels.Book {Authors = new List<BiblioSearchFB2.SearchModels.Author> { new BiblioSearchFB2.SearchModels.Author {FirstName = "Vova", LastName = "Vlad",  MiddleName = "V", NickName = "V" }, new BiblioSearchFB2.SearchModels.Author { FirstName = "Vova1", LastName = "Vlad1" } } });
            Assert.AreEqual(2, authorsActual.Count());
            Assert.AreEqual("Vova", authorsActual[0].FirstName);
            Assert.AreEqual("V", authorsActual[0].MiddleName);
            Assert.AreEqual("V", authorsActual[0].NickName);
            Assert.AreEqual("Vova1", authorsActual[1].FirstName);
            mockDbo.Verify(d => d.SearchAuthor(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
            mockDbo.Verify(d => d.AddAuthor(It.IsAny<BiblioData.DbModels.Author>()), Times.Exactly(1));

        }

        [Test]
        public void ProcessFileTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.SearchAuthor(It.IsAny<string>(), It.IsAny<string>())).Returns(new BiblioData.DbModels.Author { FirstName = "Vova", LastName = "Vlad" });
            Mock<IBiblioLog> mockBiblioLog = new Mock<IBiblioLog>();

            Mock<BiblioSearchFB2.Creator.ICreator> mockCreator = new Mock<BiblioSearchFB2.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);
            mockCreator.Setup(c => c.CreateBiblioLog()).Returns(mockBiblioLog.Object);

            var searcher = new SearchController(mockCreator.Object);

            string xmlString = "<FictionBook xmlns=\"http://www.gribuser.ru/xml/fictionbook/2.0\" xmlns:l=\"http://www.w3.org/1999/xlink\"><description><title-info>" +
                                "<genre>sf_humor</genre>" +
                                "<author><first-name>Terry</first-name><last-name>Pratchet</last-name></author><book-title>Wonderful Morris</book-title><date/>" +
                                "<annotation><p>About Cat</p></annotation><keywords>cat, rats</keywords>" +
                                "</title-info></description></FictionBook>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            searcher.ProcessFile(xmlDoc, "");
            mockBiblioLog.Verify(l => l.Info(It.IsAny<string>()), Times.Once);
            mockDbo.Verify(d => d.Save(), Times.Once);
            mockCreator.Verify(d => d.CreateDataOperation(), Times.Once);
            mockDbo.Verify(d => d.SearchGenre(It.IsAny<string>()), Times.Once);
        }
        
        [Test]
        public void ProcessFileIfRenamedTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.SearchBookByFileName(It.IsAny<string>())).Returns(new BiblioData.DbModels.Book { FileName = @"D:\Temp\test.fb2" });
            Mock<IBiblioLog> mockBiblioLog = new Mock<IBiblioLog>();

            Mock<BiblioSearchFB2.Creator.ICreator> mockCreator = new Mock<BiblioSearchFB2.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);
            mockCreator.Setup(c => c.CreateBiblioLog()).Returns(mockBiblioLog.Object);

            var searcher = new SearchController(mockCreator.Object);

            searcher.ProcessFileIfRenamed(@"D:\Temp\oldtest.fb2", @"D:\Temp\test.fb2");
            mockDbo.Verify(d => d.SearchBookByFileName(@"D:\Temp\oldtest.fb2") ,Times.Once);
            mockDbo.Verify(d => d.Save(), Times.Once);
            mockCreator.Verify(d => d.CreateDataOperation(), Times.Once);
        }

        [Test]
        public void ProcessFileIfRemovedTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.SearchBookByFileName(It.IsAny<string>())).Returns(new BiblioData.DbModels.Book { FileName = @"D:\Temp\test.fb2" });
            Mock<IBiblioLog> mockBiblioLog = new Mock<IBiblioLog>();

            Mock<BiblioSearchFB2.Creator.ICreator> mockCreator = new Mock<BiblioSearchFB2.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);
            mockCreator.Setup(c => c.CreateBiblioLog()).Returns(mockBiblioLog.Object);

            var searcher = new SearchController(mockCreator.Object);

            searcher.ProcessFileIfRemoved(@"D:\Temp\test.fb2");
            mockDbo.Verify(d => d.SearchBookByFileName(@"D:\Temp\test.fb2"), Times.Once);
            mockDbo.Verify(d => d.Save(), Times.Once);
            mockCreator.Verify(d => d.CreateDataOperation(), Times.Once);
        }

        [Test]
        public void ProcessFileWithSequenceTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.SearchAuthor(It.IsAny<string>(), It.IsAny<string>())).Returns(new BiblioData.DbModels.Author { FirstName = "Vova", LastName = "Vlad" });
            Mock<IBiblioLog> mockBiblioLog = new Mock<IBiblioLog>();

            Mock<BiblioSearchFB2.Creator.ICreator> mockCreator = new Mock<BiblioSearchFB2.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);
            mockCreator.Setup(c => c.CreateBiblioLog()).Returns(mockBiblioLog.Object);

            var searcher = new SearchController(mockCreator.Object);

            string xmlString = "<FictionBook xmlns=\"http://www.gribuser.ru/xml/fictionbook/2.0\" xmlns:l=\"http://www.w3.org/1999/xlink\"><description><title-info>" +
                                "<genre>sf_humor</genre>" +
                                "<author><first-name>Terry</first-name><last-name>Pratchet</last-name></author><book-title>Wonderful Morris</book-title><date/>" +
                                "<annotation><p>About Cat</p></annotation><keywords>cat, rats</keywords>" +
                                "<sequence name=\"Discworld\" number = \"28\"/></title-info></description></FictionBook>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            searcher.ProcessFile(xmlDoc, "");
            mockBiblioLog.Verify(l => l.Info(It.IsAny<string>()), Times.Once);
            mockDbo.Verify(d => d.Save(), Times.Once);
            mockCreator.Verify(d => d.CreateDataOperation(), Times.Once);
            mockDbo.Verify(d => d.SearchSequence(It.IsAny<string>()), Times.Once);
        }


    }
}
