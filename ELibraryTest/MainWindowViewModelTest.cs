﻿using ELibrary.ViewModels;
using NUnit.Framework;
using Moq;
using ELibrary.Creator;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ELibrary.ViewModels.DockWindows.Catalogues;
using ELibrary.ViewModels.DockWindows.References;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibraryTest
{
    [TestFixture]
    public class MainWindowViewModelTest
    {
        public MainWindowViewModelTest()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        [Test]
        public void ConstructorTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);

            Assert.IsNotNull(result.DockManagerViewModel);
        }

        [Test]
        public void AddNewCatBooksFormCommandTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);
            result.AddNewCatBooksFormCommand.Execute(null);

            mockDockManager.Verify(d => d.AddDockWndow(It.IsAny<CatBooksDockWindowViewModel>()), Times.Once);
        }

        [Test]
        public void AddNewCatAuthorsFormCommandTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);
            result.AddNewCatAuthorsFormCommand.Execute(null);

            mockDockManager.Verify(d => d.AddDockWndow(It.IsAny<CatAuthorsDockWindowViewModel>()), Times.Once);
        }

        [Test]
        public void AddNewRefAuthorsFormCommandTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);
            result.AddNewRefAuthorsFormCommand.Execute(null);

            mockDockManager.Verify(d => d.AddDockWndow(It.IsAny<RefAuthorsDockWindowViewModel>()), Times.Once);
        }

        [Test]
        public void AddNewRefGenresFormCommandTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);
            result.AddNewRefGenresFormCommand.Execute(null);

            mockDockManager.Verify(d => d.AddDockWndow(It.IsAny<RefGenresDockWindowViewModel>()), Times.Once);
        }

        [Test]
        public void AddNewRefSequencesFormCommandTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);

            var result = new MainWindowViewModel(mockCreator.Object);
            result.AddNewRefSequencesFormCommand.Execute(null);

            mockDockManager.Verify(d => d.AddDockWndow(It.IsAny<RefSequencesDockWindowViewModel>()), Times.Once);
        }

        [Test]
        public void SearchBookTest()
        {
            var mockDockManager = new Mock<IDockManagerViewModel>();
            var mockCreator = new Mock<ICreator>();
            mockCreator.Setup(c => c.CreateDockManagerViewModel()).Returns(mockDockManager.Object);
            var result = new MainWindowViewModel(mockCreator.Object);
            result.SearchBook.Execute(null);

            mockDockManager.Verify(c => c.AddDockWndow(It.IsAny<SearchWindowViewModel>()));
        }

    }
}
