﻿using System;
using NUnit.Framework;
using Moq;
using BiblioData.Context;
using System.Collections.Generic;
using System.Linq;
using ELibrary.ViewModels.DockWindows.Catalogues;
using ELibrary.Mapper;

namespace ELibraryTest
{
    [TestFixture]
    public class CatAuthorsDockWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.GetMasterGroupAuthors()).Returns((new List<BiblioData.DbModels.MasterGroupAuthor> { new BiblioData.DbModels.MasterGroupAuthor { MasterAuthorId = 1, FirstName = "First" } }).AsQueryable());

            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var catBooks = new CatAuthorsDockWindowViewModel(mockCreator.Object);

            Assert.IsFalse(catBooks.IsClosed);
            Assert.AreEqual(1, catBooks.Authors.Count());
            Assert.AreEqual("First", catBooks.Authors[0].FirstName);
            mockDbo.Verify(d => d.GetMasterGroupAuthors(), Times.Once);
        }

        [Test]
        public void AuthorDetailCommandTest()
        {

            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            var author = new BiblioData.DbModels.MasterGroupAuthor() { MasterAuthorId = 1, FirstName = "First" };

            mockDbo.Setup(d => d.GetMasterGroupAuthors()).Returns((new List<BiblioData.DbModels.MasterGroupAuthor>
                                                          {
                                                               author
                                                          }
                                                    ).AsQueryable());

            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var catAuthor = new CatAuthorsDockWindowViewModel(mockCreator.Object);
            int MasterAuthorId = 0;
            catAuthor.Commands.NewDockWindowRequested += (s, e) => MasterAuthorId = e.Id;
            catAuthor.Commands.AuthorDetailCommand.Execute(catAuthor.Authors[0]);

            Assert.AreEqual(1, MasterAuthorId);
        }
    }
}
