﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Models;
using ELibrary.Models.CatBookForm;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class CatBookTest
    {
        [Test]
        public void AuthorsFullNameTest()
        {
            var book = new CatBook()
            {
                BookId = 2,
                Title = "Second",
                Authors = new List<AuthorForCatBook>
                {
                    new AuthorForCatBook()
                    {
                        AuthorId = 2,
                        FirstName = "S"
                    },
                    new AuthorForCatBook()
                    {
                        AuthorId = 3,
                        FirstName = "T",
                        MiddleName ="TM",
                        LastName = "TL"
                    },
                    new AuthorForCatBook()
                    {
                        AuthorId = 4,
                        LastName = "FL"
                    }
                }
            };

            Assert.AreEqual("S, T TM TL, FL", book.AuthorsFullName);
        }
    }
}
