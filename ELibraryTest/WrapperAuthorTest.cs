﻿using ELibrary.Models;
using ELibrary.ViewModels.DockWindows.Recent;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class WrapperAuthorTest
    {
        [Test]
        public void AuthorIdTest()
        {
            IAuthor actual = new WrapperAuthor(new MenuClickRecentDocEventArgs(1, "type"));

            Assert.AreEqual(1, actual.AuthorId);
        }
    }
}
