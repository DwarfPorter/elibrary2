﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class RelayCommandTest
    {
        [Test]
        public void ConstructorTest()
        {
            object actualExecute = "";
            object actualCanExecute = "";

            var cmd = new RelayCommand(o => { actualExecute = o; }, (o) => { actualCanExecute = o; return true; });

            bool IsCanExecute = cmd.CanExecute("222");
            cmd.Execute("333");

            Assert.IsTrue(IsCanExecute);
            Assert.AreEqual("222", actualCanExecute);
            Assert.AreEqual("333", actualExecute);

        }
    }
}
