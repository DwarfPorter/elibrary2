﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels.DockWindows.Recent;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class RecentDocTest
    {
        [Test]
        public void ConstructorTest()
        {
            RecentDoc actual = new RecentDoc(1, 2, "type", "name");

            Assert.AreEqual(1, actual.Number);
            Assert.AreEqual(2, actual.Id);
            Assert.AreEqual("type", actual.Type);
            Assert.AreEqual("name", actual.Name);
        }
    }
}
