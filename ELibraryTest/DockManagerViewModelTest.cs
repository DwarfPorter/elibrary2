﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class DockManagerViewModelTest
    {

        [Test]
        public void ConstructorTest()
        {
            var result = new DockManagerViewModel();

            Assert.IsNotNull(result.Documents);
        }

        [Test]
        public void AddDockWndowTest()
        {
            var result = new DockManagerViewModel();

            var doc = new MockDockWindowViewModel();

            bool IsActiveDocumentExecute = false;
            result.PropertyChanged += (s, o) => { if (o.PropertyName == "ActiveDocument") IsActiveDocumentExecute = true; };

            result.AddDockWndow(doc);

            Assert.AreSame(doc, result.Documents[0]);
            Assert.AreSame(doc, result.ActiveDocument);
            Assert.IsTrue(IsActiveDocumentExecute);

        }

        [Test]
        public void Document_PropertyChangedTest_IsClosed()
        {
            var result = new DockManagerViewModel();

            var doc = new MockDockWindowViewModel();

            result.AddDockWndow(doc);
            doc.Close();

            Assert.AreEqual(0, result.Documents.Count);

        }

        [Test]
        public void Document_NewDockWindowRequestedTest()
        {
            var result = new DockManagerViewModel();
            var doc = new Mock2DockWindowViewModel();

            result.AddDockWndow(doc);
            doc.OnNewDockWindowRequestedTest(1);

            Assert.IsInstanceOf<Mock3DockWindowViewModel>(result.Documents[1]);
            Assert.IsInstanceOf<Mock3DockWindowViewModel>(result.ActiveDocument);
            Assert.AreEqual(1, ((Mock3DockWindowViewModel)result.ActiveDocument).Id);

        }



        public class MockDockWindowViewModel : DockWindowViewModel
        {

        }

        public class Mock2DockWindowViewModel : DockWindowViewModel
        {
            public void OnNewDockWindowRequestedTest(int id)
            {
                Commands.OnNewDockWindowRequested<Mock3DockWindowViewModel>(id);
            }
        }

        public class Mock3DockWindowViewModel : DockWindowViewModel
        {
            public int Id;
            public Mock3DockWindowViewModel(int id) : base() { Id = id; }
        }
    }
}
