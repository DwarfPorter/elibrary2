﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class DockWindowEventArgsTest
    {
        [Test]
        public void ConstructorTest()
        {
            var result = new DockWindowEventArgs(typeof(MockDockWindowViewModel), 1);

            Assert.AreEqual(typeof(MockDockWindowViewModel), result.TypeDockWindowViewModel);
            Assert.AreEqual(1, result.Id);

        }

        [Test]
        public void ConstructorGenericTest()
        {
            var result = new DockWindowEventArgs<MockDockWindowViewModel>(1);

            Assert.AreEqual(typeof(MockDockWindowViewModel), result.TypeDockWindowViewModel);
            Assert.AreEqual(1, result.Id);
        }

        public class MockDockWindowViewModel : DockWindowViewModel
        {

        }

    }
}
