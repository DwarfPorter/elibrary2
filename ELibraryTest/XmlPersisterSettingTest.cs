﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.Settings;
using Moq;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class XmlPersisterSettingTest
    {
        [Test]
        public void SaveLoadCoreTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);

            var setting = repo.Create<ISettings>();
            setting.SetupGet(s => s.FileNameGuiSetting).Returns("");

            var guisetting = new GuiSetting { RecentDocs = new List<SettingRecentDoc> { new SettingRecentDoc(1, 1, "type", "name"), new SettingRecentDoc(2, 2, "type", "имя2") } };
            var xmlPersister = new XmlPersisterSetting(setting.Object);
            var strActual = xmlPersister.SaveCore(guisetting);
            var actual = xmlPersister.LoadCore(strActual);

            Assert.AreEqual(guisetting.RecentDocs[0].Id, actual.RecentDocs[0].Id);
            Assert.AreEqual(guisetting.RecentDocs[0].Number, actual.RecentDocs[0].Number);
            Assert.AreEqual(guisetting.RecentDocs[0].Type, actual.RecentDocs[0].Type);
            Assert.AreEqual(guisetting.RecentDocs[0].Name, actual.RecentDocs[0].Name);

            Assert.AreEqual(guisetting.RecentDocs[1].Id, actual.RecentDocs[1].Id);
            Assert.AreEqual(guisetting.RecentDocs[1].Number, actual.RecentDocs[1].Number);
            Assert.AreEqual(guisetting.RecentDocs[1].Type, actual.RecentDocs[1].Type);
            Assert.AreEqual(guisetting.RecentDocs[1].Name, actual.RecentDocs[1].Name);


        }

    }
}
