﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiblioData.Context;
using Moq;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class SearcherTest
    {
        [Test]
        public void FindTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.SearchAuthorsByMask(It.IsAny<string>())).Returns(new List<BiblioData.DbModels.Author>
            {
                new BiblioData.DbModels.Author {AuthorId = 1, FirstName = "Jhon", LastName = "Silveran", MiddleName="Golden", NickName="Brilliant" },
                new BiblioData.DbModels.Author {AuthorId = 2, FirstName= "Silverin", LastName = "LastName2", MiddleName = "Superb" },
                new BiblioData.DbModels.Author {AuthorId = 3, FirstName = "GGG", MiddleName="Silvereen", LastName="Last" },
                new BiblioData.DbModels.Author {AuthorId = 4, NickName = "Silveration" }
            }.AsQueryable());

            mockDbo.Setup(d => d.SearchBooksByMask(It.IsAny<string>())).Returns(new List<BiblioData.DbModels.Book>
            {
                new BiblioData.DbModels.Book {BookId = 5, Title = "Silver A", Annotation = "Bug Silver"},
                new BiblioData.DbModels.Book {BookId = 6, Title= "Ver", Annotation = "My Silver River" }
            }.AsQueryable());

            mockDbo.Setup(d => d.SearchBooksByContent(It.IsAny<string>())).Returns(new List<BiblioData.DbModels.BookContent>
            {
                new BiblioData.DbModels.BookContent {BookId = 7, Title = "Silmaer", Content = "Bug Silver 2"}
            }.AsQueryable());
            
            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var searcher = new ELibrary.ViewModels.DockWindows.Search.Searcher(mockCreator.Object);
            var Actual = searcher.Find("Silver", ELibrary.ViewModels.DockWindows.Search.SearchType.Author | ELibrary.ViewModels.DockWindows.Search.SearchType.Book | ELibrary.ViewModels.DockWindows.Search.SearchType.Content);

            mockDbo.Verify(d => d.SearchAuthorsByMask("Silver"));
            mockDbo.Verify(d => d.SearchBooksByMask("Silver"));
            mockDbo.Verify(d => d.SearchBooksByContent("Silver"));

            Assert.AreEqual(7, Actual.Count());
            Assert.IsTrue(Actual.Any(a => a.Content == "Silveran" && a.Id == 1 && a.Type == "Автор"));
            Assert.IsTrue(Actual.Any(a => a.Content == "Silverin" && a.Id == 2 && a.Type == "Автор"));
            Assert.IsTrue(Actual.Any(a => a.Content == "Silvereen" && a.Id == 3 && a.Type == "Автор"));
            Assert.IsTrue(Actual.Any(a => a.Content == "Silveration" && a.Id == 4 && a.Type == "Автор"));

            Assert.IsTrue(Actual.Any(a => a.Content == "Silver A" && a.Id == 5 && a.Type == "Книга"));
            Assert.IsTrue(Actual.Any(a => a.Content == "My Silver River" && a.Id == 6 && a.Type == "Книга"));

            Assert.IsTrue(Actual.Any(a => a.Content == "Bug Silver 2" && a.Id == 7 && a.Type == "Оглавление"));

        }
    }
}
