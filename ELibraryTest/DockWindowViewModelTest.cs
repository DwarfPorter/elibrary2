﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELibrary.ViewModels;
using Moq;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class DockWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {
            var mockDockWin = new Mock<DockWindowViewModel>();

            var result = mockDockWin.Object;
            Assert.IsFalse(result.IsClosed);
        }

        [Test]
        public void CloseTest()
        {
            var result = new MockDockWindowViewModel();

            bool flExecClose = false;
            result.PropertyChanged += (s, e) => { if (e.PropertyName == "IsClosed") flExecClose = true; };
            result.Close();

            Assert.IsTrue(result.IsClosed);
            Assert.IsTrue(flExecClose);
        }

        [Test]
        public void TitleTest()
        {
            var result = new MockDockWindowViewModel();

            bool flExecTitle = false;
            result.PropertyChanged += (s, e) => { if (e.PropertyName == "Title") flExecTitle = true; };
            result.Title = "First";

            Assert.AreEqual("First", result.Title);
            Assert.IsTrue(flExecTitle);
        }

        [Test]
        public void CloseCommandTest()
        {
            var mockDockWin = new Mock<DockWindowViewModel>();
            var result = mockDockWin.Object;

            result.CloseCommand.Execute(null);

            Assert.IsTrue(result.IsClosed);

        }

        public class MockDockWindowViewModel : DockWindowViewModel
        {

        }

    }

}
