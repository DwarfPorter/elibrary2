﻿using NUnit.Framework;
using Moq;
using BiblioData.Context;
using System.Collections.Generic;
using System.Linq;
using ELibrary.ViewModels.DockWindows.Catalogues;

namespace ELibraryTest
{
    [TestFixture]
    public class BookDockWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.GetBook(It.IsAny<int>())).Returns(new BiblioData.DbModels.Book
            {
                BookId = 1,
                Title = "First",
                GenreId = 1,
                Genre = new BiblioData.DbModels.Genre { GenreId = 1, Code = "G1"},
                Sequence = new BiblioData.DbModels.Sequence { SequenceId = 1, Name = "S"},
                Authors = new List<BiblioData.DbModels.Author>
                                                                        {
                                                                            new BiblioData.DbModels.Author()
                                                                            {
                                                                                AuthorId = 1,
                                                                                FirstName = "F"
                                                                            }
                                                                        }
            });

            Mock<ELibrary.Creator.ICreator> mockCreator = new Mock<ELibrary.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);
            var mockPersister = new Mock<ELibrary.Settings.IPersisterGuiSetting>();
            mockPersister.Setup(p => p.Load()).Returns(new ELibrary.Settings.GuiSetting());

            mockCreator.Setup(c => c.CreatePersisterGuiSetting()).Returns(mockPersister.Object);

            var book = new BookDockWindowViewModel(mockCreator.Object, 1);

            Assert.IsFalse(book.IsClosed);
            Assert.AreEqual("First", book.Book.Title);
            Assert.AreEqual("Книга 'First'", book.Title);
            Assert.AreEqual("G1", book.Book.GenreCode);
            Assert.AreEqual("S", book.Book.SequenceName);


        }
    }
}
