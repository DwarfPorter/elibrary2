﻿using ELibrary.Models;
using ELibrary.Models.BookForm;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class AuthorForBookTest
    {
        [Test]
        public void FullNameTest()
        {
            var result1 = new AuthorForBook()
            {
                AuthorId = 3,
                FirstName = "T",
                MiddleName = "TM",
                LastName = "TL"
            };

            Assert.AreEqual("T TM TL", result1.FullName);

            var result2 = new AuthorForBook()
            {
                AuthorId = 3,
                FirstName = "T"
            };

            Assert.AreEqual("T", result2.FullName);

            var result3 = new AuthorForBook()
            {
                AuthorId = 3,
                MiddleName = "TM"
            };

            Assert.AreEqual("TM", result3.FullName);

            var result4 = new AuthorForBook()
            {
                AuthorId = 3,
                LastName = "TL"
            };

            Assert.AreEqual("TL", result4.FullName);

            var result5 = new AuthorForBook()
            {
                AuthorId = 3,
                MiddleName = "TM",
                LastName = "TL"
            };

            Assert.AreEqual("TM TL", result5.FullName);

            var result6 = new AuthorForBook()
            {
                AuthorId = 3,
                FirstName = "T",
                LastName = "TL"
            };

            Assert.AreEqual("T TL", result6.FullName);

        }
    }
}
