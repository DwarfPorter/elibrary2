﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using ELibrary.ViewModels;
using ELibrary.ViewModels.DockWindows;
using ELibrary.ViewModels.DockWindows.Search;

namespace ELibraryTest
{
    [TestFixture]
    public class SearchWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {

            var mockCreator = new Mock<ELibrary.Creator.ICreator>();
            var mockPersister = new Mock<ELibrary.Settings.IPersisterGuiSetting>();
            mockPersister.Setup(p => p.Load()).Returns(new ELibrary.Settings.GuiSetting());

            mockCreator.Setup(c => c.CreatePersisterGuiSetting()).Returns(mockPersister.Object);


            var result = new SearchWindowViewModel(mockCreator.Object);

            Assert.IsNotNull(result.Search);
        }

        [Test]
        public void SearchCommandTest()
        {
            var mockSearcher = new Mock<ISearcher>();
            var mockCreator = new Mock<ELibrary.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateSearcher()).Returns(mockSearcher.Object);
            var mockPersister = new Mock<ELibrary.Settings.IPersisterGuiSetting>();
            mockPersister.Setup(p => p.Load()).Returns(new ELibrary.Settings.GuiSetting());

            mockCreator.Setup(c => c.CreatePersisterGuiSetting()).Returns(mockPersister.Object);
            var mockSearchItem = new Mock<ELibrary.Models.SearchForm.ISearchItem>();
            mockSearcher.Setup(s => s.Find(It.IsAny<string>(), It.IsAny<SearchType>())).Returns(new List<ELibrary.Models.SearchForm.ISearchItem> { mockSearchItem.Object });

            var result = new SearchWindowViewModel(mockCreator.Object);
            result.SearchCommand.Execute(null);

            mockSearcher.Verify(d => d.Find(It.IsAny<string>(), It.IsAny<SearchType>()), Times.Once);

        }
    }
}
