﻿using System;
using NUnit.Framework;
using Moq;
using BiblioData.Context;
using System.Collections.Generic;
using System.Linq;
using ELibrary.ViewModels.DockWindows.Catalogues;
using ELibrary.Mapper;

namespace ELibraryTest
{
    [TestFixture]
    public class CatBooksDockWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {

            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.GetBooks()).Returns((new List<BiblioData.DbModels.Book>
                                                          {
                                                                new BiblioData.DbModels.Book()
                                                                {
                                                                    BookId = 1,
                                                                    Title = "First",
                                                                    GenreId = 1,
                                                                    Genre = new BiblioData.DbModels.Genre { GenreId = 1, Code ="G1" },
                                                                    Sequence = new BiblioData.DbModels.Sequence { SequenceId = 1, Name ="S" },
                                                                    Authors = new List<BiblioData.DbModels.Author>
                                                                        {
                                                                            new BiblioData.DbModels.Author()
                                                                            {
                                                                                AuthorId = 1,
                                                                                FirstName = "F"
                                                                            }
                                                                        }
                                                                },
                                                                new BiblioData.DbModels.Book()
                                                                {
                                                                    BookId = 2,
                                                                    Title = "Second",
                                                                    GenreId = 1,
                                                                    Genre = new BiblioData.DbModels.Genre { GenreId = 1, Code ="G1" },
                                                                    Sequence = new BiblioData.DbModels.Sequence { SequenceId = 1, Name ="S" },
                                                                    Authors = new List<BiblioData.DbModels.Author>
                                                                        {
                                                                            new BiblioData.DbModels.Author()
                                                                            {
                                                                                AuthorId = 2,
                                                                                FirstName = "S"
                                                                            },
                                                                            new BiblioData.DbModels.Author()
                                                                            {
                                                                                AuthorId = 3,
                                                                                FirstName = "T",
                                                                                MiddleName ="TM",
                                                                                LastName = "TL"
                                                                            },
                                                                            new BiblioData.DbModels.Author()
                                                                            {
                                                                                AuthorId = 4,
                                                                                LastName = "FL"
                                                                            }
                                                                    }
                                                                }
                                                            }
                                                    ).AsQueryable());

            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var catBooks = new CatBooksDockWindowViewModel(mockCreator.Object);

            Assert.IsFalse(catBooks.IsClosed);
            Assert.AreEqual(2, catBooks.Books.Count());
            Assert.AreEqual("First", catBooks.Books[0].Title);
            Assert.AreEqual("S, T TM TL, FL", catBooks.Books[1].AuthorsFullName);
            mockDbo.Verify(d => d.GetBooks(), Times.Once);
        }

        [Test]
        public void ConstructorTestWithoutAuthors()
        {

            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.GetBooks()).Returns((new List<BiblioData.DbModels.Book>
                                                          {
                                                                new BiblioData.DbModels.Book()
                                                                {
                                                                    BookId = 1,
                                                                    Title = "First",
                                                                    GenreId = 1,
                                                                    Genre = new BiblioData.DbModels.Genre { GenreId = 1, Code ="G1" },
                                                                    Sequence = new BiblioData.DbModels.Sequence { SequenceId = 1, Name ="S" }

                                                                }
                                                          }
                                                    ).AsQueryable());

            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var catBooks = new CatBooksDockWindowViewModel(mockCreator.Object);

            Assert.IsFalse(catBooks.IsClosed);
            Assert.AreEqual(1, catBooks.Books.Count());
            Assert.AreEqual("First", catBooks.Books[0].Title);
            mockDbo.Verify(d => d.GetBooks(), Times.Once);
        }

        [Test]
        public void BookDetailCommandTest()
        {

            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            var book = new BiblioData.DbModels.Book()
            {
                BookId = 1,
                Title = "First",
                GenreId = 1,
                Genre = new BiblioData.DbModels.Genre { GenreId = 1, Code = "G1" },
                Sequence = new BiblioData.DbModels.Sequence { SequenceId = 1, Name = "S" }

            };
            mockDbo.Setup(d => d.GetBooks()).Returns((new List<BiblioData.DbModels.Book>
                                                          {
                                                               book
                                                          }
                                                    ).AsQueryable());

            Mock<BiblioData.Creator.ICreator> mockCreator = new Mock<BiblioData.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var catBooks = new CatBooksDockWindowViewModel(mockCreator.Object);
            int bookId = 0;
            catBooks.Commands.NewDockWindowRequested += (s, e) => bookId = e.Id;
            catBooks.Commands.BookDetailCommand.Execute(catBooks.Books[0]);

            Assert.AreEqual(1, bookId);
        }






    }
}
