﻿using ELibrary.Models;
using ELibrary.ViewModels.DockWindows.Recent;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class WrapperBookTest
    {
        [Test]
        public void BookIdTest()
        {
            IBook actual = new WrapperBook(new MenuClickRecentDocEventArgs(1, "type"));

            Assert.AreEqual(1, actual.BookId);
        }
    }
}
