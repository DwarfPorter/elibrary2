﻿using ELibrary.ViewModels.DockWindows.Recent;
using NUnit.Framework;

namespace ELibraryTest
{
    [TestFixture]
    public class MenuClickRecentDocEventArgsTest
    {
        [Test]
        public void ConstructorTest()
        {
            MenuClickRecentDocEventArgs actual = new MenuClickRecentDocEventArgs(1, "type");

            Assert.AreEqual(1, actual.Id);
            Assert.AreEqual("type", actual.Type);
        }
    }
}
