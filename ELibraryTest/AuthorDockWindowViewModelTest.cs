﻿using NUnit.Framework;
using Moq;
using BiblioData.Context;
using System.Collections.Generic;
using System.Linq;
using ELibrary.ViewModels.DockWindows.Catalogues;

namespace ELibraryTest
{
    [TestFixture]
    public class AuthorDockWindowViewModelTest
    {
        [Test]
        public void ConstructorTest()
        {
            Mock<IDataOperation> mockDbo = new Mock<IDataOperation>();
            mockDbo.Setup(d => d.GetMasterGroupAuthor(It.IsAny<int>())).Returns(new BiblioData.DbModels.MasterGroupAuthor
            {
                MasterAuthorId = 1,
                FirstName = "First",
                LastName = "Last",
            });
            var books = (new List<BiblioData.DbModels.Book>() { new BiblioData.DbModels.Book { BookId = 1, Title = "book" } }).AsQueryable();
            mockDbo.Setup(d => d.GetBooksByMasterAuthor(It.IsAny<int>())).Returns(books);


            Mock<ELibrary.Creator.ICreator> mockCreator = new Mock<ELibrary.Creator.ICreator>();
            mockCreator.Setup(c => c.CreateDataOperation()).Returns(mockDbo.Object);

            var mockPersister = new Mock<ELibrary.Settings.IPersisterGuiSetting>();
            mockPersister.Setup(p => p.Load()).Returns(new ELibrary.Settings.GuiSetting());

            mockCreator.Setup(c => c.CreatePersisterGuiSetting()).Returns(mockPersister.Object);

            var author = new AuthorDockWindowViewModel(mockCreator.Object, 1);

            Assert.IsFalse(author.IsClosed);
            Assert.AreEqual("First", author.Author.FirstName);



        }
    }
}
